def login_API(self,emails):
        # Your login logic here
        for i in range(0, 5):
            login_url = f"{self.baseURL}api/user/login"

            login_data = {
                "grant_type": "password",
                "username": "dummy25jgebt6945",
                "email": emails,  # Assuming email is hardcoded for now
                "password": "abcdef",
                "scope": "*",
                "network": "",
                "access_token": "",
            }

            login_response = self.client.post(
                url=login_url,
                name='1. Login',
                timeout=120,
                allow_redirects=False,
                headers={'Accept': 'application/json, text/plain, */*'},
                json=login_data
            )
            print('status code is =', login_response.status_code)
            if login_response.status_code == 200 and 'user' in login_response.json() and 'id' in login_response.json()['user']:
                time.sleep(self.sleep_time)
                self.access_token = login_response.json()['access_token']
                self.player_id = login_response.json()['user']['id']
                break
            elif i < 4:
                print("Login_API Failed at" + ' time-' + str(i), login_response.text)
            else:
                print("Login_API Failed permanently at ", login_response.text)
                self.user.environment.events.request_failure.fire(
                request_type="POST",
                name="1. Login",
                response_time=login_response.elapsed.total_seconds() * 1000,
                response_length=len(login_response.content),
                exception=Exception(f"Login_API Failed permanently at: {login_response.text}"),
                )
                time.sleep(999999999990)

def profile_true_API(self):

    for z in range(0, 5):

        profile_true_URL = f"{self.baseURL}api/user/profile?e=true"
        query_params = {'e' : True}
        
        profile_true_URL_response = self.client.get(
                url=profile_true_URL,
                name = '4. Profile_true_API',
                timeout=120,
                allow_redirects=False,
                headers={'Accept': 'application/json, text/plain, */*', 'Authorization': f'Bearer {self.access_token}'},
                params = query_params
            )

        if profile_true_URL_response.status_code == 200 and "data" in profile_true_URL_response.json():
            time.sleep(self.sleep)
            break

        elif z < 4:
            print(f"profile_true API failed"  + " time=" + str(z), {profile_true_URL_response.text})
            time.sleep(self.sleep)

        else:
            print(f"profile_true API Permanently failed", {profile_true_URL_response.text})
            time.sleep(9999999000000)

def getUserPermissions_API(self):

    for z in range(0, 5):

        getUserPermissions_URL = f"{self.baseURL}api/getUserPermissions"

        getUserPermissions_URL_response = self.client.get(
            url=getUserPermissions_URL,
            name = '4. getUserPermissions_API',
            timeout=120,
            allow_redirects=False,
            headers={'Accept': 'application/json, text/plain, */*', 'Authorization': f'Bearer {self.access_token}'}
        )

        if getUserPermissions_URL_response.status_code == 200 and 'data' in getUserPermissions_URL_response.json():
            time.sleep(self.sleep)
            break

        elif z < 4:
            print(f"getUserPermissions API failed due to :"  + " time = " + str(z), {getUserPermissions_URL_response.text})
            time.sleep(self.sleep)

        else:
            print(f"getUserPermissions API Permanent failed : {getUserPermissions_URL_response.text}")
            time.sleep(9999999000000)

def getMyWatchlist_API(self):

    for z in range(0, 5):

        getMyWatchlist_URL = f"{self.baseURL}api/getMyWatchlist"

        getMyWatchlist_URL_response = self.client.get(
            url=getMyWatchlist_URL,
            name = '5. getMyWatchlist_API',
            timeout=120,
            allow_redirects=False,
            headers={'Accept': 'application/json, text/plain, */*', 'Authorization': f'Bearer {self.access_token}'}
        )

        if getMyWatchlist_URL_response.status_code == 200 and 'data' in getMyWatchlist_URL_response.json():
            time.sleep(self.sleep)
            break

        elif z < 4:
            print(f"getMyWatchlist API failed due to :"  + " time=" + str(z), {getMyWatchlist_URL_response.text})
            time.sleep(self.sleep)

        else:
            print(f"getMyWatchlist API permanent failed due to : {getMyWatchlist_URL_response.text}")
            time.sleep(9999999000000)

def get_user_coins_API(self):

        for z in range(0, 5):

            get_user_coins_URL = f"{self.baseURL}api/get-user-coins"

            get_user_coins_URL_response = self.client.get(
                url=get_user_coins_URL,
                name = '4. get_user_coins_API',
                timeout=120,
                allow_redirects=False,
                headers={'Accept': 'application/json, text/plain, */*', 'Authorization': f'Bearer {self.access_token}'}
            )

            if get_user_coins_URL_response.status_code == 200 and 'status' in get_user_coins_URL_response.json():
                time.sleep(self.sleep) 
                break

            elif z < 4:
                print(f"get_user_coins API failed due to :"  + " time=" + str(z), {get_user_coins_URL_response.text})
                time.sleep(self.sleep)
                

            else:
                print(f"get_user_coins API failed due to : {get_user_coins_URL_response.text}")
                time.sleep(9999999000000)

def upcomingRoundsSuggestion_API(self):

        for z in range(0, 5):

            Suggestion_API_URL = f"{self.baseURL}api/user/get-upcoming-rounds-suggestion?group=all"
            queryParam_data = {'group' : all }

            Suggestion_API_URL_response = self.client.get(
                url=Suggestion_API_URL,
                name = '4. UpcomingRoundsSuggestion_API',
                timeout=120,
                allow_redirects=False,
                headers={'Accept': 'application/json, text/plain, */*', 'Authorization': f'Bearer {self.access_token}'},
                coin_params = queryParam_data
            )

            if Suggestion_API_URL_response.status_code == 200 and 'data' in Suggestion_API_URL_response.json():
                time.sleep(self.sleep) 
                break

            elif z < 4:
                print(f"upcomingRoundsSuggestion API failed due to :"  + " time=" + str(z), {Suggestion_API_URL_response.text})
                time.sleep(self.sleep)
                

            else:
                print(f"upcomingRoundsSuggestion API failed due to : {Suggestion_API_URL_response.text}")
                time.sleep(9999999000000)

def get_all_featured_API(self):

        for z in range(0, 5):

            get_all_featured_URL = f"{self.baseURL}api/public/get-all-featured"

            get_all_featured_response = self.client.get(
                url=get_all_featured_URL,
                name = '4. Get_all_featured_API',
                timeout=120,
                allow_redirects=False,
                headers={'Accept': 'application/json, text/plain, */*', 'Authorization': f'Bearer {self.access_token}'}
            )

            if get_all_featured_response.status_code == 200 and 'data' in get_all_featured_response.json():
                time.sleep(self.sleep) 
                break

            elif z < 4:
                print(f"get_all_featured API failed due to :"  + " time=" + str(z), {get_all_featured_response.text})
                time.sleep(self.sleep)
                

            else:
                print(f"get_all_featured API failed due to : {get_all_featured_response.text}")
                time.sleep(9999999000000)

def Homepage_API(self):

        for z in range(0, 5):

            Homepage_URL = f"{self.baseURL}api/public/get-banners/homepage"

            Homepage_API_response = self.client.get(
                url=Homepage_URL,
                name = '4. Homepage_API',
                timeout=120,
                allow_redirects=False,
                headers={'Accept': 'application/json, text/plain, */*', 'Authorization': f'Bearer {self.access_token}'}
            )

            if Homepage_API_response.status_code == 200 and 'data' in Homepage_API_response.json():
                time.sleep(self.sleep) 
                break

            elif z < 4:
                print(f"Homepage API failed due to :"  + " time=" + str(z), {Homepage_API_response.text})
                time.sleep(self.sleep)
                

            else:
                print(f"Homepage API failed due to : {Homepage_API_response.text}")
                time.sleep(9999999000000)

def upcomingRegistered_API(self):

        for z in range(0, 5):

            upcomingRegistered_URL = f"{self.baseURL}api/personalize/upcoming-registered"

            upcomingRegistered_response = self.client.get(
                url=upcomingRegistered_URL,
                name = '4. UpcomingRegistered_API',
                timeout=120,
                allow_redirects=False,
                headers={'Accept': 'application/json, text/plain, */*', 'Authorization': f'Bearer {self.access_token}'}
            )

            if upcomingRegistered_response.status_code == 200 and 'data' in upcomingRegistered_response.json():
                time.sleep(self.sleep) 
                break

            elif z < 4:
                print(f"upcomingRregistered API failed due to :"  + " time=" + str(z), {upcomingRegistered_response.text})
                time.sleep(self.sleep)
                

            else:
                print(f"upcomingRregistered API failed due to : {upcomingRegistered_response.text}")
                time.sleep(9999999000000)

