import email
from email.message import EmailMessage
import json
from requests_toolbelt.multipart.encoder import MultipartEncoder
import time
import requests
import datetime
import random
from locust import HttpUser, SequentialTaskSet, task, between, constant_pacing
from csvreader import CSVReader
import os
import re
import csv

# email_reader = CSVReader("User_Details.csv")  # Assuming CSVReader is implemented

# sleep_time = 10

fileName = os.path.basename(__file__)
numbers = re.findall('[0-9]+', fileName)

def seq_task(_):
    return task


def fetch_emails_from_csv_url(csv_url):
    response = requests.get(csv_url)
    response.raise_for_status()  # Ensure we notice bad responses
    emails = []
    # Decode the content and parse CSV
    decoded_content = response.content.decode('utf-8').splitlines()
    reader = csv.reader(decoded_content)
    for row in reader:
        emails.append(row[0])  # Assuming email is in the first column
    return emails

def CurrentTimeZone():
    # Get current datetime in the desired timezone
    formatted_current_dt = datetime.datetime.now(datetime.timezone(datetime.timedelta(hours=5, minutes=30))).strftime('%Y-%m-%d %H:%M:%S%z')

    # Insert colon between hours and minutes in the timezone offset
    formatted_current_dt = formatted_current_dt[:-2] + ':' + formatted_current_dt[-2:]

    return formatted_current_dt # Output: Current date and time in the specified format

def RandomOrganization():
        """This will generate a random organization for the player"""
        OrganizationList = [
            {"id": 483, "name": "Guru Gobind Singh Indraprastha University (GGSIPU), Delhi"},
            {"id": 591, "name": "Indian Institute of Management (IIM), Ranchi"},
            {"id": 597, "name": "Indian Institute of Management (IIM), Udaipur"},
            {"id": 122804, "name": "Global Institutes of Management (GIM), Amritsar"},
            {"id": 614, "name": "Indian Institute of Technology (IIT), Indore"},
            {"id": 122804, "name": "Global Institutes of Management (GIM), Amritsar"},
            {"id": 604, "name": "Indian Institute of Technology (IIT), Bombay"},
            {"id": 608, "name": "Indian Institute of Technology (IIT),  Roorkee"},
            {"id": 597, "name": "Indian Institute of Management (IIM), Udaipur"},
            {"id": 122804, "name": "Global Institutes of Management (GIM), Amritsar"}]

        organization = random.choice(OrganizationList)
        return organization

def RandomPlayerName():
    """This will generate a random player name for the given team"""
    alphaChoice = ["A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "Q", "R", "S",
                    "T", "U", "V", "W", "X", "Y", "Z"]
    playername = "Player" + random.choice(alphaChoice) + random.choice(alphaChoice) + str(random.randint(1, 9999))
    return playername
    
def RandomGender():
    """This will generate a random gender from the available list"""
    GenderList = ['M', 'F', 'I', 'O', 'NB', 'T']
    Gender = random.choice(GenderList)
    return Gender

def generate_audio_data(audio_file_urls):
    # Select a random audio file URL from the list
    random_audio_url = random.choice(audio_file_urls)
    
    # Construct the Audio_data dictionary with the randomly selected file URL
    Audio_data = {
        "file_name": "audio-1714123604.webm",
        "file_type": "audio/webm",
        "upload_directory": "upload/proctoring/recordings/stage/707950/19220923/6920016a7c70884d308d92efc2ce4a43/audio/46020974",
        "file_url": random_audio_url  # Add the random file URL to the dictionary
    }
    
    return Audio_data

def create_event_data(round_id, sEventType):
    Event_data = {
        "iEntityId": round_id,
        "sEntityType": "App\\Models\\AssessmentRound\\AssessmentRound",
        "sEventType": sEventType,
        "sDateTime": CurrentTimeZone()
    }
    return Event_data

# Function to download image from URL and return binary data
def download_image_as_binary():
    image_url = 'https://d8it4huxumps7.cloudfront.net/files/6643039ba5ac8_face.webp'
    response = requests.get(image_url)
    response.raise_for_status()  # Check if the request was successful
    return response.content


class MyUser(SequentialTaskSet):
    baseURL = 'https://stage-v1.d2c.pw/'
    assessment_round_id = 6996 #12481 #11969  # 18976
    compId = 50340
    sleep = 1

    def __init__(self, *args, **kwargs):
        # Fetch emails from the CSV URL
        csv_url = "https://d8it4huxumps7.cloudfront.net/files/664338d5e1da6_file0.csv"
        emails_list = fetch_emails_from_csv_url(csv_url)

        # Use the first email for this instance
        self.emails = emails_list[0]  # Adjust indexing as needed for your use case
        # csvemail = next(email_reader)
        # self.emails = csvemail[0]
        # self.mobile = csvemail[2]
        super().__init__(*args, **kwargs)
        self.access_token = None
        self.sleep_time = 5
        
        self.audio_file_urls = [
                "https://d8it4huxumps7.cloudfront.net/files/662b77b4a73cb_sample_7.webm",
                "https://d8it4huxumps7.cloudfront.net/files/662b77c190c9a_sample_5.webm",
                "https://d8it4huxumps7.cloudfront.net/files/662b77ce681b3_sample_5_1.webm",
                "https://d8it4huxumps7.cloudfront.net/files/662b77d7e8752_sample_0.webm"
                ]

        self.login_API(self.emails)
        # self.emails = None

    #Login API's
        
    def login_API(self,emails):
        # Your login logic here
        for i in range(0, 5):
            login_url = f"{self.baseURL}api/user/login"

            login_data = {
                "grant_type": "password",
                "username": "dummy25jgebt6945",
                "email": emails,  # Assuming email is hardcoded for now
                "password": "abcdef",
                "scope": "*",
                "network": "",
                "access_token": "",
            }

            login_response = self.client.post(
                url=login_url,
                name='1. Login',
                timeout=120,
                allow_redirects=False,
                headers={'Accept': 'application/json, text/plain, */*'},
                json=login_data
            )
            print('status code is =', login_response.status_code)
            if login_response.status_code == 200 and 'user' in login_response.json() and 'id' in login_response.json()['user']:
                time.sleep(self.sleep_time)
                self.access_token = login_response.json()['access_token']
                self.player_id = login_response.json()['user']['id']
                break
            elif i < 4:
                print("Login_API Failed at" + ' time-' + str(i), login_response.text)
            else:
                print("Login_API Failed permanently at ", login_response.text)
                time.sleep(999999999990)

    def profile_true_API(self):

        for z in range(0, 5):

            profile_true_URL = f"{self.baseURL}api/user/profile?e=true"
            query_params = {'e' : True}
            
            profile_true_URL_response = self.client.get(
                    url=profile_true_URL,
                    name = '2. Profile_true_API',
                    timeout=120,
                    allow_redirects=False,
                    headers={'Accept': 'application/json, text/plain, */*', 'Authorization': f'Bearer {self.access_token}'},
                    params = query_params
                )

            if profile_true_URL_response.status_code == 200 and "data" in profile_true_URL_response.json():
                time.sleep(self.sleep)
                break

            elif z < 4:
                print(f"profile_true API failed"  + " time=" + str(z), {profile_true_URL_response.text})
                time.sleep(self.sleep)

            else:
                print(f"profile_true API Permanently failed", {profile_true_URL_response.text})
                time.sleep(9999999000000)

    def getUserPermissions_API(self):

        for z in range(0, 5):

            getUserPermissions_URL = f"{self.baseURL}api/getUserPermissions"

            getUserPermissions_URL_response = self.client.get(
                url=getUserPermissions_URL,
                name = '3. getUserPermissions_API',
                timeout=120,
                allow_redirects=False,
                headers={'Accept': 'application/json, text/plain, */*', 'Authorization': f'Bearer {self.access_token}'}
            )

            if getUserPermissions_URL_response.status_code == 200 and 'data' in getUserPermissions_URL_response.json():
                time.sleep(self.sleep)
                break

            elif z < 4:
                print(f"getUserPermissions API failed due to :"  + " time = " + str(z), {getUserPermissions_URL_response.text})
                time.sleep(self.sleep)

            else:
                print(f"getUserPermissions API Permanent failed : {getUserPermissions_URL_response.text}")
                time.sleep(9999999000000)

    def getMyWatchlist_API(self):

        for z in range(0, 5):

            getMyWatchlist_URL = f"{self.baseURL}api/getMyWatchlist"

            getMyWatchlist_URL_response = self.client.get(
                url=getMyWatchlist_URL,
                name = '4. getMyWatchlist_API',
                timeout=120,
                allow_redirects=False,
                headers={'Accept': 'application/json, text/plain, */*', 'Authorization': f'Bearer {self.access_token}'}
            )

            if getMyWatchlist_URL_response.status_code == 200 and 'data' in getMyWatchlist_URL_response.json():
                time.sleep(self.sleep)
                break

            elif z < 4:
                print(f"getMyWatchlist API failed due to :"  + " time=" + str(z), {getMyWatchlist_URL_response.text})
                time.sleep(self.sleep)

            else:
                print(f"getMyWatchlist API permanent failed due to : {getMyWatchlist_URL_response.text}")
                time.sleep(9999999000000)

    def get_user_coins_API(self):

        for z in range(0, 5):

            get_user_coins_URL = f"{self.baseURL}api/get-user-coins"

            get_user_coins_URL_response = self.client.get(
                url=get_user_coins_URL,
                name = '5. get_user_coins_API',
                timeout=120,
                allow_redirects=False,
                headers={'Accept': 'application/json, text/plain, */*', 'Authorization': f'Bearer {self.access_token}'}
            )

            if get_user_coins_URL_response.status_code == 200 and 'status' in get_user_coins_URL_response.json():
                time.sleep(self.sleep) 
                break

            elif z < 4:
                print(f"get_user_coins API failed due to :"  + " time=" + str(z), {get_user_coins_URL_response.text})
                time.sleep(self.sleep)
                

            else:
                print(f"get_user_coins API failed due to : {get_user_coins_URL_response.text}")
                time.sleep(9999999000000)
    
    def upcomingRoundsSuggestion_API(self):

            for z in range(0, 5):

                Suggestion_API_URL = f"{self.baseURL}api/user/get-upcoming-rounds-suggestion?group=all"
                queryParam_data = {'group':'all' }

                Suggestion_API_URL_response = self.client.get(
                    url=Suggestion_API_URL,
                    name = '6. UpcomingRoundsSuggestion_API',
                    timeout=120,
                    allow_redirects=False,
                    headers={'Accept': 'application/json, text/plain, */*', 'Authorization': f'Bearer {self.access_token}'},
                    params = queryParam_data
                )

                if Suggestion_API_URL_response.status_code == 200 and 'data' in Suggestion_API_URL_response.json():
                    time.sleep(self.sleep) 
                    break

                elif z < 4:
                    print(f"upcomingRoundsSuggestion API failed due to :"  + " time=" + str(z), {Suggestion_API_URL_response.text})
                    time.sleep(self.sleep)
                    

                else:
                    print(f"upcomingRoundsSuggestion API failed due to : {Suggestion_API_URL_response.text}")
                    time.sleep(9999999000000)

    def get_all_featured_API(self):

            for z in range(0, 5):

                get_all_featured_URL = f"{self.baseURL}api/public/get-all-featured"

                get_all_featured_response = self.client.get(
                    url=get_all_featured_URL,
                    name = '7. Get_all_featured_API',
                    timeout=120,
                    allow_redirects=False,
                    headers={'Accept': 'application/json, text/plain, */*', 'Authorization': f'Bearer {self.access_token}'}
                )

                if get_all_featured_response.status_code == 200 and 'data' in get_all_featured_response.json():
                    time.sleep(self.sleep) 
                    break

                elif z < 4:
                    print(f"get_all_featured API failed due to :"  + " time=" + str(z), {get_all_featured_response.text})
                    time.sleep(self.sleep)
                    

                else:
                    print(f"get_all_featured API failed due to : {get_all_featured_response.text}")
                    time.sleep(9999999000000)

    def Homepage_API(self):

            for z in range(0, 5):

                Homepage_URL = f"{self.baseURL}api/public/get-banners/homepage"

                Homepage_API_response = self.client.get(
                    url=Homepage_URL,
                    name = '8. Homepage_API',
                    timeout=120,
                    allow_redirects=False,
                    headers={'Accept': 'application/json, text/plain, */*', 'Authorization': f'Bearer {self.access_token}'}
                )

                if Homepage_API_response.status_code == 200 and 'data' in Homepage_API_response.json():
                    time.sleep(self.sleep) 
                    break

                elif z < 4:
                    print(f"Homepage API failed due to :"  + " time=" + str(z), {Homepage_API_response.text})
                    time.sleep(self.sleep)
                    

                else:
                    print(f"Homepage API failed due to : {Homepage_API_response.text}")
                    time.sleep(9999999000000)

    def upcomingRegistered_API(self):

            for z in range(0, 5):

                upcomingRegistered_URL = f"{self.baseURL}api/personalize/upcoming-registered"

                upcomingRegistered_response = self.client.get(
                    url=upcomingRegistered_URL,
                    name = '4. UpcomingRegistered_API',
                    timeout=120,
                    allow_redirects=False,
                    headers={'Accept': 'application/json, text/plain, */*', 'Authorization': f'Bearer {self.access_token}'}
                )

                if upcomingRegistered_response.status_code == 200 and 'data' in upcomingRegistered_response.json():
                    time.sleep(self.sleep) 
                    break

                elif z < 4:
                    print(f"upcomingRregistered API failed due to :"  + " time=" + str(z), {upcomingRegistered_response.text})
                    time.sleep(self.sleep)
                    

                else:
                    print(f"upcomingRregistered API failed due to : {upcomingRegistered_response.text}")
                    time.sleep(9999999000000)


    #API's called after search opp on Explore page just after login
    #Global API called twice before and after Get_global_search_API
    def Global_API(self):

            for z in range(0, 5):

                Global_URL = f"{self.baseURL}api/get-user-recent-search/global"

                Global_API_response = self.client.get(
                    url=Global_URL,
                    name = '9. Global_Explore_API',
                    timeout=120,
                    allow_redirects=False,
                    headers={'Accept': 'application/json, text/plain, */*', 'Authorization': f'Bearer {self.access_token}'}
                )

                if Global_API_response.status_code == 200 and 'data' in Global_API_response.json():
                    time.sleep(self.sleep) 
                    break

                elif z < 4:
                    print(f"Global API failed due to :"  + " time=" + str(z), {Global_API_response.text})
                    time.sleep(self.sleep)
                    

                else:
                    print(f"Global API failed due to : {Global_API_response.text}")
                    time.sleep(9999999000000)

    def Get_global_search_API(self):

            for z in range(0, 5):

                Get_global_search_URL = f"{self.baseURL}api/get-global-search-data?searchTerm=Opportunity%20name"
                Query_globalData = {'searchTerm': 'Opportunity name'}

                Get_global_search_response = self.client.get(
                    url=Get_global_search_URL,
                    name = '10. Get_global_search_API',
                    timeout=120,
                    allow_redirects=False,
                    headers={'Accept': 'application/json, text/plain, */*', 'Authorization': f'Bearer {self.access_token}'},
                    params=Query_globalData
                )

                if Get_global_search_response.status_code == 200 and 'data' in Get_global_search_response.json():
                    time.sleep(self.sleep) 
                    break

                elif z < 4:
                    print(f"Get_global_search API failed due to :"  + " time=" + str(z), {Get_global_search_response.text})
                    time.sleep(self.sleep)
                    

                else:
                    print(f"Get_global_search API failed due to : {Get_global_search_response.text}")
                    time.sleep(9999999000000)

    def Get_banner_API(self):

            for z in range(0, 5):

                Get_banner_API_URL = f"{self.baseURL}api/public/ad-banner/get-banner?type=explore-flyer&isUserPro=0"
                

                Get_banner_API_response = self.client.get(
                    url=Get_banner_API_URL,
                    name = '11. Get_banner_API',
                    timeout=120,
                    allow_redirects=False,
                    headers={'Accept': 'application/json, text/plain, */*', 'Authorization': f'Bearer {self.access_token}'},
                    params={
                        "type": "explore-flyer",
                        "isUserPro": "0"
                    }
                )

                if Get_banner_API_response.status_code == 200 and 'data' in Get_banner_API_response.json():
                    time.sleep(self.sleep) 
                    break

                elif z < 4:
                    print(f"Get_banner API failed due to :"  + " time=" + str(z), {Get_banner_API_response.text})
                    time.sleep(self.sleep)
                    

                else:
                    print(f"Get_banner API failed due to : {Get_banner_API_response.text}")
                    time.sleep(9999999000000)

    def City_name_API(self):

            for z in range(0, 5):

                City_name_API_URL = f"{self.baseURL}api/public/city-name?cache-bust=1712311859325"

                City_name_API_response = self.client.get(
                    url=City_name_API_URL,
                    name = '12. City_name_API',
                    timeout=120,
                    allow_redirects=False,
                    headers={'Accept': 'application/json, text/plain, */*', 'Authorization': f'Bearer {self.access_token}'},
                    params={'cache-bust': 1712311859325}
                )

                if City_name_API_response.status_code == 200 and 'data' in City_name_API_response.json():
                    time.sleep(self.sleep) 
                    break

                elif z < 4:
                    print(f"City_name API failed due to :"  + " time=" + str(z), {City_name_API_response.text})
                    time.sleep(self.sleep)
                    

                else:
                    print(f"City_name API failed due to : {City_name_API_response.text}")
                    time.sleep(9999999000000)

    def Search_result_API(self):

            for z in range(0, 5):

                Search_result_URL = f"{self.baseURL}api/public/opportunity/search-result?opportunity=all&page=1&per_page=15&searchTerm=Opportunity%20name&oppstatus=recent"

                Search_result_response = self.client.get(
                    url=Search_result_URL,
                    name = '13. Search_result_API',
                    timeout=120,
                    allow_redirects=False,
                    headers={'Accept': 'application/json, text/plain, */*', 'Authorization': f'Bearer {self.access_token}'},
                    params={'opportunity': 'all',
                    'page': 1,
                    'per_page': 15,
                    'searchTerm': 'Opportunity name',
                    'oppstatus': 'recent'})
                
                
                if Search_result_response.status_code == 200 and 'data' in Search_result_response.json():
                    time.sleep(self.sleep) 
                    self.id_list = []
                    json_data = Search_result_response.json()
                    # Extract IDs
                    for item in json_data['data']['data']:
                        self.id_list.append(item['id'])
                    break

                elif z < 4:
                    print(f"Search_result API failed due to :"  + " time=" + str(z), {Search_result_response.text})
                    time.sleep(self.sleep)
                    

                else:
                    print(f"Search_result API failed due to : {Search_result_response.text}")
                    time.sleep(9999999000000)

    def Result_opp_API(self):

            for z in range(0, 5):

                Result_opp_URL = f"{self.baseURL}api/public/competition/{self.id_list[0]}"

                Result_opp_response = self.client.get(
                    url=Result_opp_URL,
                    name = '14. Result_opp_API',
                    timeout=120,
                    allow_redirects=False,
                    headers={'Accept': 'application/json, text/plain, */*', 'Authorization': f'Bearer {self.access_token}'})

                if Result_opp_response.status_code == 200 and 'data' in Result_opp_response.json():
                    time.sleep(self.sleep) 
                    break

                elif z < 4:
                    print(f"Result_opp API failed due to :"  + " time=" + str(z), {Result_opp_response.text})
                    time.sleep(self.sleep)
                    

                else:
                    print(f"Result_opp API failed due to : {Result_opp_response.text}")
                    time.sleep(9999999000000)

    def Check_player_status_API(self,OppID):

            for z in range(0, 5):

                Check_player_status_URL = f"{self.baseURL}api/opportunity/{OppID}/check-player-status"

                Check_player_status_response = self.client.get(
                    url=Check_player_status_URL,
                    name = '15. Check_player_status_API',
                    timeout=120,
                    allow_redirects=False,
                    headers={'Accept': 'application/json, text/plain, */*', 'Authorization': f'Bearer {self.access_token}'})

                if Check_player_status_response.status_code == 200 and 'data' in Check_player_status_response.json():
                    time.sleep(self.sleep) 
                    break

                elif z < 4:
                    print(f"Check_player_status API failed due to :"  + " time=" + str(z), {Check_player_status_response.text})
                    time.sleep(self.sleep)
                    

                else:
                    print(f"Check_player_status API failed due to : {Check_player_status_response.text}")
                    time.sleep(9999999000000)

    def Update_views_API(self):

            for z in range(0, 5):

                Update_views_URL = f"{self.baseURL}microservices/d2c-analytics/api/views/update-views"
                Update_data = {
                "from_competition_page": None,
                "home_exp_views": {
                    "user_id": self.player_id,
                    "entity_ids": [
                    self.id_list[0],
                    self.id_list[1],
                    self.id_list[2],
                    self.id_list[3],
                    self.id_list[4],
                    self.id_list[5],
                    self.id_list[6],
                    self.id_list[7],
                    self.id_list[8],
                    self.id_list[9]
                    ],
                    "entity_type": "opportunity"
                },
                "festival_views": None,
                "articles_views": None,
                "current_page_url": "https://unstop.com/all-opportunities",
                "current_version": "5.10.2024.18.24"
                }

                Update_views_response = self.client.post(
                    url=Update_views_URL,
                    name = '16. Update_views_API',
                    timeout=120,
                    allow_redirects=False,
                    headers={'Accept': 'application/json, text/plain, */*', 'Authorization': f'Bearer {self.access_token}'},
                    json=Update_data
                    )

                if Update_views_response.status_code == 200 and 'status' in Update_views_response.json():
                    time.sleep(self.sleep) 
                    break

                elif z < 4:
                    print(f"Update_views API failed due to :"  + " time=" + str(z), {Update_views_response.text})
                    time.sleep(self.sleep)
                    

                else:
                    print(f"Update_views API failed due to : {Update_views_response.text}")
                    time.sleep(9999999000000)

    def opp_API(self):

        for i in range(0, 5):
            opp_url = f"{self.baseURL}api/public/competition/{self.compId}"
            opp_response = self.client.get(
                url=opp_url,
                name='17. OppPage_API',
                timeout=120,
                allow_redirects=False,
                headers={'Accept': 'application/json, text/plain, */*', 'Authorization': f'Bearer {self.access_token}'}
            )

            if opp_response.status_code == 200 and 'data' in opp_response.json():
                print(opp_response.status_code)
                time.sleep(self.sleep_time)
                # oppPage = opp_response.json()
                # Convert the string to a Python dictionary
                
                # Iterate through each item in the API response
                # for item in oppPage['data']['competition']['rounds']:
                #     # Check if the 'entity_type' is 'App\\Model\\Quiz'
                #     if item.get('entity_type') == 'App\\Model\\Quiz':
                #         # Extract and store 'id' and 'entity_id'
                #         self.quiz_id = item.get('id') #611652
                #         self.quiz_entity_id = item.get('entity_id') #247191
                #         break
                #         # Break the loop once the first matching item is found
                    
                #     elif item.get('entity_type') == 'App\\Model\\CodeContests':

                #         # Extract and store 'id' and 'entity_id'
                #         self.code_id = item.get('id') #615908
                #         self.code_entity_id = item.get('entity_id') #112940
                #         break
                #         # Break the loop once the first matching item is found
                #     else:
                #         pass
                #         # time.sleep(999999999990)
        
                # self.proctoring_data = opp_response.json()['data']['competition']['regnRequirements']['proctoring']
                break

            elif i < 4:
                print(f"opp_url API failed " + ' time-' + str(i), {opp_response.text})
                time.sleep(2)

            else:
                print("opp_url API permanently failed " + ' time-' + str(i), opp_response.text)
                time.sleep(999999999990)
    
    def register_API(self, emails, mobile):
        # Your register logic here

        for i in range(0, 5):
            registration_url = f"{self.baseURL}api/registration/team/{self.compId}/create"
            Organization = RandomOrganization()

            registration_data = {"team": {"team_name": None,"players": [{
                "id": None,
                "name": RandomPlayerName(),
                "mobile": mobile,
                "email": emails,
                "organisationName": Organization['name'],
                "others": 11,
                "organisation": {
                    "id": Organization['id'],
                    "name": Organization['name'],
                    "logo": None,        
                    "public_url": "c/s-infinity-recruiter-career-interview-selection-process-job-profile-articles-videos-105107",
                },
                "player_type": 1,
                "gender": RandomGender(),
                "location": "India",
                "differently_abled": 0,
                "formValidStatus": True,
                "user_id": self.player_id,
                "player_valid": 1,
                "organisation_id": Organization['id'],
                "user_type": 4,
                "extended_basic_form1": None
            }
            ],
                    "formValidStatus": True,
                    "subscription": 1
            },
                "subscription": 1,
                "source_url": f"/competitions/{self.compId}/register",
                "client_id": "f1e92e4f-6eff-4424-998a-5ea397468a3f",
                "time_zone": "Asia/Calcutta"
            }

            registration_response = self.client.post(
                url=registration_url,
                name='3. Registration',
                timeout=120,
                allow_redirects=False,
                headers={'Accept': 'application/json, text/plain, */*', 'Authorization': f'Bearer {self.access_token}'},
                json=registration_data
            )
            

            if registration_response.status_code == 200 and 'data' in registration_response.json():
                time.sleep(self.sleep_time)
                if 'face' in self.proctoring_data and self.proctoring_data['face'] is True:
                    self.register_face_API()
                else:
                    # If the key is not present, handle it accordingly
                    print("face proctoring is not present in this opportunity.") 
                break  

            elif i < 4:
                print(f"Registration_API failed " + ' time-' + str(i), {registration_response.text})

            else:
                print(f"Registration_API permanently failed "  + ' time-' + str(i), {registration_response.text})
                time.sleep(999999999990)

    def register_face_API(self):
        
        image_url = 'https://d8it4huxumps7.cloudfront.net/uploads/images/guidelines1.png'

        # Download the image from the URL
        response = requests.get(image_url)

        # Check if the download was successful (status code 200)
        if response.status_code == 200:
            time.sleep(self.sleep_time)
            is_player = True

        # image_data = open(image_url, 'rb').read()
        # Create a MultipartEncoder for the payload
            multipart_data = MultipartEncoder(
                fields={
                    'image': ('image.png', response.content, 'image/png'),
                    'competitionId': str(self.compId),
                    'is_player': str(is_player),
                }
            )
            headers = {'Content-Type': multipart_data.content_type,'Accept': 'application/json, text/plain, */*', 'Authorization': f'Bearer {self.access_token}'}

            
        else:
            time.sleep(self.sleep_time)
            print(f"Failed to download image from {image_url}")

        for i in range(0, 5):
            
            register_face_url = f"{self.baseURL}api/proctoring/registration-image-edit"

            register_face_response = self.client.post(
                url = register_face_url,
                name='3(a). Register_face',
                timeout=120,
                allow_redirects=False,
                headers=headers,
                data = multipart_data
            )

            if register_face_response.status_code == 200 and 'data' in register_face_response.json():
                time.sleep(self.sleep_time)
                break

            elif i < 4:
                print(f"Face Registration_API  failed " + ' time-' + str(i), {register_face_response.text})
            
            else:
                print(f"Face Registration_API permanently failed " + ' time-' + str(i), {register_face_response.text})
                time.sleep(999999999990)

    def assessment_round_Api(self):

        for z in range(0, 5):

            Assessment_URL = f"{self.baseURL}api/assessment2/assessment/{self.assessment_round_id}"
            
            #Play Api start here:
            Assessment_URL_response = self.client.get(
                    url=Assessment_URL,
                    name = '18. Assessment_API',
                    timeout=120,
                    allow_redirects=False,
                    headers={'Accept': 'application/json, text/plain, */*', 'Authorization': f'Bearer {self.access_token}'}
                )
            

            if Assessment_URL_response.status_code == 200 and "data" in Assessment_URL_response.json():
                time.sleep(self.sleep)
                break

            elif z < 4:
                print(f"Assessment_round API failed"  + " time=" + str(z), {Assessment_URL_response.text})
                time.sleep(self.sleep)

            else:
                print(f"Assessment_round API Permanently failed", {Assessment_URL_response.text})
                time.sleep(9999999000000)   

    def play_Api(self):

        for z in range(0, 5):

            Play_URL = f"{self.baseURL}api/assessment2/assessment/{self.assessment_round_id}/play?"
            
            #Play Api start here:
            Play_URL_response = self.client.get(
                    url=Play_URL,
                    name = '19. Play_API',
                    timeout=120,
                    allow_redirects=False,
                    headers={'Accept': 'application/json, text/plain, */*', 'Authorization': f'Bearer {self.access_token}'}
                )
            

            if Play_URL_response.status_code == 200 and "data" in Play_URL_response.json():
                time.sleep(self.sleep)
                self.playApi_data = Play_URL_response.json()
                self.question_ID = Play_URL_response.json()['data']['questions']
                self.proctoring = Play_URL_response.json()['data']['quiz']['proctoring']
                
                # Convert the JSON-formatted string to a Python dictionary
                data = Play_URL_response.json()
                self.question_id_count = len([(question.keys()) for question in data['data']['questions']])
                break

            elif z < 4:
                print(f"Play API failed"  + " time=" + str(z), {Play_URL_response.text})
                time.sleep(self.sleep)

            else:
                print(f"Play API Permanently failed", {Play_URL_response.text})
                time.sleep(9999999000000)
    
    def save_proctoring_API(self):

        Proct_data_ip = {
            "association_id": self.assessment_round_id,
            "coordinate": "[28.5680842,77.1887089]",
            "round_type": "App\\Models\\AssessmentRound\\AssessmentRound",
            "ipaddress": True
            }

        for z in range(0, 5):

            Proct_URL = f"{self.baseURL}api/proctoring/save-proctoring-data"

            proct_response = self.client.post(
                url=Proct_URL,
                name= '20. save_proctoring_API',
                timeout=120,
                allow_redirects=False,  
                headers={'Accept': 'application/json, text/plain, */*', 'Authorization': f'Bearer {self.access_token}'},
                json=Proct_data_ip
            )

            if proct_response.status_code == 200 and 'status' in proct_response.json():
                time.sleep(self.sleep)
                break
            
            elif z < 4:
                print(f"Proctoring_save API failed "  + "time=" + str(z), {proct_response.text})
                time.sleep(self.sleep)

            else:
                print(f"Proctoring_save API Permanent failed due to : {proct_response.text}")
                time.sleep(9999999000000)

    def event(self,json_data):

        for z in range(0, 5):

            Event_URL = f"{self.baseURL}api/proctoring/save/round/event"

            event_response = self.client.post(
                url=Event_URL,
                name= '21. Event_API',
                timeout=120,
                allow_redirects=False,  
                headers={'Accept': 'application/json, text/plain, */*', 'Authorization': f'Bearer {self.access_token}'},
                json=json_data
            )

            if event_response.status_code == 200 and 'status' in event_response.json():
                time.sleep(self.sleep)
                break
            
            elif z < 4:
                print(f"Event_API failed "  + "time=" + str(z), {event_response.text})
                time.sleep(self.sleep)

            else:
                print(f"Event_API API Permanent failed due to : {event_response.text}")
                time.sleep(9999999000000)

    def faceVer_duringRound(self):

        for z in range(0, 5):

            faceVer_duringRound_URL = f"{self.baseURL}api/face-verification/during-round/{self.assessment_round_id}"
            query_paramss = {'type': 'assessmentnewround',
            'time': CurrentTimeZone(),
            'folderPath': 'production/732917/3787979/14653ade10dcf4a96503f2e1a7de0304/face',
            'imageName': 'image-1715666071.webp'}

            faceVer_duringRound_response = self.client.post(
                url=faceVer_duringRound_URL,
                name= '22. faceVer_duringRound',
                timeout=120,
                allow_redirects=False,  
                headers={'Accept': 'application/json, text/plain, */*', 'Authorization': f'Bearer {self.access_token}'},
                data=download_image_as_binary(),
                params = query_paramss
            )

            if faceVer_duringRound_response.status_code == 200 and 'status' in faceVer_duringRound_response.json():
                time.sleep(self.sleep)
                break
            
            elif z < 4:
                print(f"Event_API failed "  + "time=" + str(z), {faceVer_duringRound_response.text})
                time.sleep(self.sleep)

            else:
                print(f"Event_API API Permanent failed due to : {faceVer_duringRound_response.text}")
                time.sleep(9999999000000)

    def upload_screenshot(self):

        for z in range(0, 5):

            upload_screenshot_URL = f"{self.baseURL}api/proctoring/upload-screenshot"
            query_paramss = {'screenshotTime': CurrentTimeZone(),
            'type': 'assessmentnewround',
            'id': self.assessment_round_id}

            upload_screenshot_response = self.client.post(
                url=upload_screenshot_URL,
                name= '23. Upload_screenshot_Proct',
                timeout=120,
                allow_redirects=False,  
                headers={'Accept': 'application/json, text/plain, */*', 'Authorization': f'Bearer {self.access_token}'},
                data=download_image_as_binary(),
                params = query_paramss
            )

            if upload_screenshot_response.status_code == 200 and 'data' in upload_screenshot_response.json():
                print('Data for upload ss is = ',upload_screenshot_response.json()['data'])
                time.sleep(self.sleep)
                break
            
            elif z < 4:
                print(f"Upload_screenshot API Proctoring while playing Assessment failed "  + "time=" + str(z), {upload_screenshot_response.text})
                time.sleep(self.sleep)

            else:
                print(f"Upload_screenshot API Proctoring while playing Assessment, Permanent failed due to : {upload_screenshot_response.text}")
                time.sleep(9999999000000)


    def Answer_API(self, question_id, answer_payload, y):   #answer

        for z in range(0, 5):

            AnsURL = f"{self.baseURL}api/assessment2/assessment/{self.assessment_round_id}/answer/{question_id}"
                
            AnsURL_response = self.client.post(
                url=AnsURL,
                name=f'{24 + y}. Answer_API- {y}',
                timeout=120,
                allow_redirects=False,
                headers={'Accept': 'application/json, text/plain, */*', 'Authorization': f'Bearer {self.access_token}'},
                data=answer_payload     
            )

            if AnsURL_response.status_code == 200 and 'data' in AnsURL_response.json():
                time.sleep(self.sleep)
                break

            elif z < 4:
                print(f"Answer_API response is failed with"  + " time=" + str(z),  AnsURL_response.text)
                time.sleep(self.sleep)

            else:
                print("Answer_API response is failed" + " time=" + str(z), AnsURL_response.text)
                time.sleep(9999999000000)

    def Question_API(self, question_id, y):

        for z in range(0, 5):
            Question_URL = f"{self.baseURL}api/assessment2/assessment/{self.assessment_round_id}/question/{question_id}"
            
            question_response = self.client.get(
                url= Question_URL,
                name=f'{24+y}. Question_API- {y}',
                timeout=120,
                allow_redirects=False,
                headers={'Accept': 'application/json, text/plain, */*', 'Authorization': f'Bearer {self.access_token}'}
            )

            if question_response.status_code == 200 and 'id' in question_response.json()['data']['question']:
                time.sleep(self.sleep)
                self.question_Api_data = question_response.json()
                break
               
            elif z < 4:
                print(f"Question_API", self.question_Api_data['data']['question']['id'], "failed "  + " time=" + str(z), {question_response.text})
                time.sleep(self.sleep)

            else:
                print("Question_API response is failed" + " time=" + str(z), question_response.text)
                time.sleep(9999999000000)
   
    def play_Quiz(self):
        
        #answer api

        answers_choices = ["choice1", "choice2", "choice3"]
        # Choose a random key from the list of choices_keys
        answer = random.choice(answers_choices)

        if self.question_id_count == 1:
            firstQuestion = self.playApi_data['data']["currentQuestion"] #get first question data to use in first answer payload
            firstQuestion['answer'] = self.playApi_data['data']["currentQuestion"]['details'][answer]
            answer_payload = firstQuestion #store the response on new variable
            # self.audio_proctoring_API()
            self.Answer_API(self.playApi_data['data']["currentQuestion"]['question_id'], answer_payload, 0)

        elif self.question_id_count > 1 :
            firstQuestion = self.playApi_data['data']["currentQuestion"] #get first question data to use in first answer payload
            self.event(create_event_data(self.assessment_round_id, "tab_change_detect")) #tab change voilation count event added
            firstQuestion['answer'] = self.playApi_data['data']["currentQuestion"]['details'][answer]
            answer_payload = firstQuestion #store the response on new variable
            # self.audio_proctoring_API()
            self.Answer_API(self.playApi_data['data']["currentQuestion"]['question_id'], answer_payload, 0)

            z = 1
            for question_id in range(1, self.question_id_count):
                self.Question_API(self.question_ID[question_id]['question_id'], z)
                play_data = self.playApi_data["data"]["questions"]

                if play_data:
                    currentQuestion = self.playApi_data['data']['questions'][question_id] #get first question data to use in first answer payload
                    currentQuestion['answer'] = self.question_Api_data['data']["question"][answer]
                    answer_payload = currentQuestion
                    # self.audio_proctoring_API()
                    self.Answer_API(self.question_ID[question_id]['question_id'], answer_payload, z)
                    time.sleep(random.randint(1, 2))
                    z+=1
            # self.audio_proctoring_API()
            
        else:
            print("No Questions present in this Assessment round")
            time.sleep(9999999000000)
        
    def finish_API(self):
        #finish API's

        finish_payload = {
            "finish_type": "finishBtn",
            "is_demo": False
            }

        for z in range(0, 5):

            finish_response = self.client.post(
                url= f"{self.baseURL}api/assessment2/assessment/{self.assessment_round_id}/finish",
                name='Finish_API',
                timeout=120,
                allow_redirects=False,
                headers={'Accept': 'application/json, text/plain, */*', 'Authorization': f'Bearer {self.access_token}'},
                json=finish_payload,
            )

            if finish_response.status_code == 200 and 'data' in finish_response.json():
                time.sleep(self.sleep)
                break

            elif z < 4:
                print(f"Finish API failed"   + " time=" + str(z), {finish_response.text})
                time.sleep(self.sleep)
            
            else:
                print(f"Finish API Permanently failed due to : {finish_response.text}")
                time.sleep(9999999000000)

    def Update_review(self):
        #update review
        Review_payload = {
            "review": "Load feedback",
            "rating": 5
            }

        for z in range(0, 5):

            Review_response = self.client.post(
                url= f"{self.baseURL}api/assessment2/assessment/{self.assessment_round_id}/update-review",
                name='Review_API',
                timeout=120,
                allow_redirects=False,
                headers={'Accept': 'application/json, text/plain, */*', 'Authorization': f'Bearer {self.access_token}'},
                json=Review_payload,
            )


            if Review_response.status_code == 200 and 'data' in Review_response.json():
                time.sleep(self.sleep)
                break

            elif z < 4:
                print(f"Review API failed"  + " time=" + str(z), {Review_response.text})
                time.sleep(self.sleep)

            else:
                print(f"Review API Permanently failed due to : {Review_response.text}")
                time.sleep(9999999000000)

    def get_review_setting_API(self):

        for z in range(0, 5):
            get_review_setting_API_URL = f"{self.baseURL}api/get-review-setting/{self.assessment_round_id}/assessment_round"
            
            get_review_setting_response = self.client.get(
                url= get_review_setting_API_URL,
                name=f' Get_review_setting_API',
                timeout=120,
                allow_redirects=False,
                headers={'Accept': 'application/json, text/plain, */*', 'Authorization': f'Bearer {self.access_token}'}
            )

            if get_review_setting_response.status_code == 200 and 'status' in get_review_setting_response.json():
                time.sleep(self.sleep)
                break
               
            elif z < 4:
                print(f"get_review_setting API failed"  + " time=" + str(z), {get_review_setting_response.text})
                time.sleep(self.sleep)

            else:
                print("get_review_setting API response is failed Permanently " + " time=" + str(z), get_review_setting_response.text)
                time.sleep(9999999000000)

    def audio_proctoring_API(self):
        #Reg API start:

        for z in range(0, 5):

            audio_proctoring_URL = f"{self.baseURL}api/create-presigned-url"

            audio_proctoring_response = self.client.post(
                url=audio_proctoring_URL,
                name= "8. Audio_Proctoring_API",
                timeout=120,
                allow_redirects=False,  
                headers={'Accept': 'application/json, text/plain, */*', 'Authorization': f'Bearer {self.access_token}'},
                json=generate_audio_data(self.audio_file_urls)
            )

            if audio_proctoring_response.status_code == 200:
                if 'status' in audio_proctoring_response.json():
                    print(audio_proctoring_response.json()['message'])
                    time.sleep(self.sleep)
                    break
                else:
                    print("Response doesn't contain 'status' key:", audio_proctoring_response.text)
           
            elif z < 4:
                print(f"Proctoring_save API failed "  + "time=" + str(z), {audio_proctoring_response.text})
                time.sleep(self.sleep)

            else:
                print(f"Proctoring_save API Permanent failed due to : {audio_proctoring_response.text}")
                time.sleep(9999999000000)
    
       
    @seq_task(1)
    def Load_test(self):

        # self.login_API(self.emails)
        self.profile_true_API()
        self.getUserPermissions_API()
        self.getMyWatchlist_API()
        self.get_user_coins_API()
        self.upcomingRoundsSuggestion_API()
        self.get_all_featured_API()
        self.Homepage_API()
        self.Global_API()
        self.Get_global_search_API()
        self.Global_API()
        self.Get_banner_API()
        self.City_name_API()
        self.Search_result_API()
        self.Result_opp_API()
        self.Check_player_status_API(self.id_list[0])
        self.Update_views_API() #16

        self.opp_API()

        # self.register_API(self.emails,self.mobile)

        self.assessment_round_Api()

        self.play_Api()

        self.save_proctoring_API()

        self.event(create_event_data(self.assessment_round_id, "join_exit_panel"))
        self.event(create_event_data(self.assessment_round_id, "screen_share"))
        self.faceVer_duringRound()
        self.upload_screenshot()
        self.event(create_event_data(self.assessment_round_id, "tab_change_detect"))
        self.play_Quiz()
        self.finish_API()
         #update event for finish_test
        self.event(create_event_data(self.assessment_round_id, "finish_test"))

        # get_review_setting_API
        self.get_review_setting_API()

        self.upload_screenshot()

        self.faceVer_duringRound()

        self.Update_review()

        self.opp_API()

        self.Check_player_status_API(self.compId)
        print("Finished..!")

        time.sleep(9999999999999)
        

class LocustForTesting(HttpUser):
    tasks = [MyUser]
    host = f'{MyUser.baseURL}'
    wait_time = constant_pacing(400)
    # wait_time = between(1, 2)
    weight = 1
    min_wait = 10
    max_wait = 190
