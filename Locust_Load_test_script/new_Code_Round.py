from new_User import Users
from new_Reg import Register

class Code:
    code_solution = """#include <bits/stdc++.h> 
        using namespace std;


        /* A binary tree Treenode has data,
        pointer to left child and a
        pointer to right child */
        struct TreeNode {
            int val;
            TreeNode *left;
            TreeNode *right;
            TreeNode(int x) : val(x), left(NULL), right(NULL) {}
        };


        // Function to insert nodes in level order
        TreeNode* insertLevelOrder(vector<int>& arr, TreeNode* root,
                            int i, int n)
        {
            // Base case for recursion
            if (i < n)
            {
                TreeNode* temp = new TreeNode(arr[i]);
                root = temp;


                // insert left child
                root->left = insertLevelOrder(arr,
                        root->left, 2 * i + 1, n);


                // insert right child
                root->right = insertLevelOrder(arr,
                        root->right, 2 * i + 2, n);
            }
            return root;
        }

            int ans= 0;                                     // Global ans variable
            void helper(TreeNode* root, int mx, int mn) {
                if(!root)    return;                        // root == NULL
                    
                mx= max(mx, root->val);
                mn= min(mn, root->val);
                ans= max(ans, abs(mx-mn));                  // calculation
                
                helper(root->left, mx, mn);                 // recursion call
                helper(root->right, mx, mn);        
            }
            
            
            int maxAncestorDiff(TreeNode* root) {
                int mx= INT_MIN;
                int mn= INT_MAX;
                
                helper(root, mx, mn);
                return ans;
            }



        // Driver program to test above function
        int main()
        {
            int n;
            cin>>n;
            vector<int>arr(n);
            for(int i=0;i<n;i++){
                cin>>arr[i];
            }

            sort(arr.begin(),arr.end());

            TreeNode* root = insertLevelOrder(arr, root, 0, n);

            int ans=maxAncestorDiff(root);

            cout<<ans<<endl;

            return 0;
        }"""

    def verify_code_playApi(self, User_instance, Register_instance):

        PlayStatus_Url = f"{Users.baseURL}api/opportunity/{Users.compId}/associations/{Register_instance.code_id}?withPlayStatus=true"
        play_URL = f"{Users.baseURL}api/code-contests/{Register_instance.code_entity_id}/play"
        print("URL is = ,",play_URL)

        PlayStatus_response = User_instance.client.get(
            url=PlayStatus_Url,
            name='PlayStatus_API',
            timeout=120,
            allow_redirects=False,
            headers={'Accept': 'application/json, text/plain, */*', 'Authorization': f'Bearer {User_instance.access_token}'}
        )

        if PlayStatus_response.status_code == 200:
            
            print("PlayStatus_Url API response working")

        else:
            print(f"PlayStatus_Url API failed with status code: {PlayStatus_response.status_code}")

        
        play_response = User_instance.client.get(
            url=play_URL,
            name='Play',
            timeout=120,
            allow_redirects=False,
            headers={'Accept': 'application/json, text/plain, */*', 'Authorization': f'Bearer {User_instance.access_token}'}
        )

        if play_response.status_code == 200:
            # playApi_data = play_response.json()
            # print("DDAAata ",play_response.json())
            self.question_ID_code = play_response.json()['data']['contestinfo']['challenges']
            
            # Convert the JSON-formatted string to a Python dictionary
            data = play_response.json()

            question_id_count = len(data["data"]["contestinfo"]['challenges'])
            # print("length is =", question_id_count)

            # Print the count
            print(f'Total number of questions in this rounds are : {question_id_count}')
            
        else:
            print(f"Play API failed with status code: {play_response.status_code}")
            # print("Response is =",play_response.json())
        
    def code_Answer_API(self, User_instance): #answer
        # print("ISSSs isss ==",self.question_ID[0]['relation_id'])
        rel_id = self.question_ID_code[0]['relation_id']
        
        Answer_URL = f"{Users.baseURL}api/code-contests/326142/submit/{rel_id}"

        answer_payload = {
            "source": self.code_solution,
            "lang": "2"
            }
                        
        Answer_URL_response = User_instance.client.post(
            url=Answer_URL,
            name='Code Answer API',
            timeout=120,
            allow_redirects=False,
            headers={'Accept': 'application/json, text/plain, */*', 'Authorization': f'Bearer {User_instance.access_token}'},
            data= answer_payload
        )

        if Answer_URL_response.status_code == 200:
            print(f"code_Answer_API_data is working with status code {Answer_URL_response.status_code} ")
            # code_Answer_API_data = Answer_URL_response.json()
            # print(f"Answer response data is = ",code_Answer_API_data['result'])

        else:
            print(f"Second_AnsURLresponse is failed with {Answer_URL_response.status_code}")
        
    def get_Time_info_API(self, User_instance, Register_instance):
        Time_info_API = f"{Users.baseURL}api/code-contests/{Register_instance.code_entity_id}/get-time-info"
        Time_info_API_response = User_instance.client.get(
            url=Time_info_API,
            name='get_Time_info_API',
            timeout=120,
            allow_redirects=False,
            headers={'Accept': 'application/json, text/plain, */*', 'Authorization': f'Bearer {User_instance.access_token}'}
        )

        if Time_info_API_response.status_code == 200:
            print("opp_url API response working")
        else:
            print(f"opp_url API failed with status code: {Time_info_API_response.status_code}")
    
    def finish_code_API(self, User_instance, Register_instance):
        #finish API
        finish_URL = f"{Users.baseURL}api/code-contests/{Register_instance.code_entity_id}/finish"

        finish_payload = {
        "finish_type": "finishBtn"
        }

        finish_response = User_instance.client.post(
            url= finish_URL,
            name='finish_code_API',
            timeout=120,
            allow_redirects=False,
            headers={'Accept': 'application/json, text/plain, */*', 'Authorization': f'Bearer {User_instance.access_token}'},
            json=finish_payload,
        )

        if finish_response.status_code == 200:
            finish_response_data = finish_response.json()['data']
            
            print(f"Finish API response data = ",finish_response_data)

        else:
            print(f"Finish API failed with status code: {finish_response.status_code}")

        #update review

        Review_payload = {
            "review": "Load code round feedback",
            "rating": 5
            }

        Review_response = User_instance.client.post(
            url= f"{Users.baseURL}api/code-contests/{Register_instance.code_entity_id}/update-review",
            name='Review code round Api',
            timeout=120,
            allow_redirects=False,
            headers={'Accept': 'application/json, text/plain, */*', 'Authorization': f'Bearer {User_instance.access_token}'},
            json=Review_payload,
        )


        if Review_response.status_code == 200:
            Review_response_data = Review_response.json()['data']
            
            print(f"Review API feedback is =",Review_response_data['content'])

        else:
            print(f"Review API failed with status code: {Review_response.status_code}")
