from requests_toolbelt.multipart.encoder import MultipartEncoder
import requests
from new_User import Users


class Register: 

    def __init__(self):
        self.quiz_id = None
        self.quiz_entity_id = None
        self.code_id = None
        self.code_entity_id = None

    def opp_API(self, User_instance):
        opp_url = f"{Users.baseURL}api/public/competition/{Users.compId}"
        opp_response = User_instance.client.get(
            url=opp_url,
            name='OppPage_API',
            timeout=120,
            allow_redirects=False,
            headers={'Accept': 'application/json, text/plain, */*', 'Authorization': f'Bearer {User_instance.access_token}'}
        )

        if opp_response.status_code == 200:
            oppPage = opp_response.json()
            # Convert the string to a Python dictionary
            
            # Iterate through each item in the API response
            for item in oppPage['data']['competition']['rounds']:
                # Check if the 'entity_type' is 'App\\Model\\Quiz'
                if item.get('entity_type') == 'App\\Model\\Quiz':
                    # Extract and store 'id' and 'entity_id'
                    self.quiz_id = item.get('id') #611652
                    self.quiz_entity_id = item.get('entity_id') #247191
                    # Break the loop once the first matching item is found
                
                elif item.get('entity_type') == 'App\\Model\\CodeContests':

                    # Extract and store 'id' and 'entity_id'
                    self.code_id = item.get('id') #615908
                    self.code_entity_id = item.get('entity_id') #112940
                    # Break the loop once the first matching item is found
                else:
                    print("No Code & Quiz Round is present in this opp")
    
            self.proctoring_data = opp_response.json()['data']['competition']['regnRequirements']['proctoring']
            print("Proctur data =",self.proctoring_data)
               
            print("opp_url API response working")
        else:
            print(f"opp_url API failed with status code: {opp_response.status_code}")
    
    def register_API(self, email, mobile, name, User_instance):
        # Your register logic here

        registration_url = f"{Users.baseURL}api/registration/team/{Users.compId}/create"

        registration_data = {"team": {"team_name": None,"players": [{
            "id": 5420120,
            "name": name,
            "mobile": mobile,
            "email": email,
            "organisationName": "S INFINITY",
            "others": 11,
            "organisation": {
                "id": 105107,
                "name": "S INFINITY",
                "logo": None,        
                "public_url": "c/s-infinity-recruiter-career-interview-selection-process-job-profile-articles-videos-105107",
            },
            "player_type": 1,
            "gender": "T",
            "location": "India",
            "differently_abled": 0,
            "formValidStatus": True,
            "user_id": 840943,
            "player_valid": 1,
            "organisation_id": 105107,
            "user_type": 4,
            "extended_basic_form1": None
        }
        ],
                "formValidStatus": True,
                "subscription": 1
        },
            "subscription": 1,
            "source_url": f"/competitions/{Users.compId}/register",
            "client_id": "f1e92e4f-6eff-4424-998a-5ea397468a3f",
            "time_zone": "Asia/Calcutta"
        }

        registration_response = User_instance.client.post(
            url=registration_url,
            name='Registration',
            timeout=120,
            allow_redirects=False,
            headers={'Accept': 'application/json, text/plain, */*', 'Authorization': f'Bearer {User_instance.access_token}'},
            json=registration_data
        )
        

        if registration_response.status_code == 200:
            # print(registration_response.json())
            user_id = registration_response.json()['data']['oRegisteredTeam']['user_id']
            print(f"Registration successful. User ID is: ",user_id)
            if 'face' in self.proctoring_data and self.proctoring_data['face'] is True:
                 self.register_face_API(User_instance)
            else:
                # If the key is not present, handle it accordingly
                print("face proctoring is not present in this opportunity.")   

        else:
            print(f"Registration failed with status code: {registration_response.status_code}")

        pass    
    
    def register_face_API(self,User_instance):
        
        register_face_url = f"{Users.baseURL}api/proctoring/registration-image-edit"
        
        image_url = 'https://d8it4huxumps7.cloudfront.net/uploads/images/guidelines1.png'

        # Download the image from the URL
        response = requests.get(image_url)

        # Check if the download was successful (status code 200)
        if response.status_code == 200:
            is_player = True

        # image_data = open(image_url, 'rb').read()
        # Create a MultipartEncoder for the payload
            multipart_data = MultipartEncoder(
                fields={
                    'image': ('image.png', response.content, 'image/png'),
                    'competitionId': str(Users.compId),
                    'is_player': str(is_player),
                }
            )

            headers = {'Content-Type': multipart_data.content_type,'Accept': 'application/json, text/plain, */*', 'Authorization': f'Bearer {User_instance.access_token}'}

            register_face_response = User_instance.client.post(
                url = register_face_url,
                name='Register_face',
                timeout=120,
                allow_redirects=False,
                headers=headers,
                data = multipart_data
            )
        else:
            print(f"Failed to download image from {image_url}")

        if register_face_response.status_code == 200:
            # print(registration_response.json())
            status = register_face_response.json()['data']['status']
            print("check",register_face_response.json()['data'])
            print(f"Face Register successful. status is: ",status)

        else:
            print(f"Face Registration failed with status code: {register_face_response.status_code}")

        pass

