from new_User import Users
from new_Reg import Register
import time

class Quiz:

    def __init__(self):
         self.question_id_count = None

    def verify_quiz_playApi(self, User_instance,Register_instance):
            PlayStatus_Url = f"{Users.baseURL}api/opportunity/{Users.compId}/associations/{Register_instance.quiz_id}?withPlayStatus=true"
            play_URL = f"{Users.baseURL}api/v2/quiz/{Register_instance.quiz_entity_id}/play?"
            print("firsrt URL =", play_URL)


            quiz_response = User_instance.client.get(
                url=PlayStatus_Url,
                name='Play_Verify API',
                timeout=120,
                allow_redirects=False,
                headers={'Accept': 'application/json, text/plain, */*', 'Authorization': f'Bearer {User_instance.access_token}'}
            )

            if quiz_response.status_code == 200:
                print("PlayStatus_Url API response working")
            else:
                print(f"PlayStatus_Url API failed with status code: {quiz_response.status_code}")

            
            play_response = User_instance.client.get(
                url=play_URL,
                name='Play',
                timeout=120,
                allow_redirects=False,
                headers={'Accept': 'application/json, text/plain, */*', 'Authorization': f'Bearer {User_instance.access_token}'}
            )

            if play_response.status_code == 200:
                self.playApi_data = play_response.json()
                self.question_ID = play_response.json()['data']['questions']
                
                # Convert the JSON-formatted string to a Python dictionary
                data = play_response.json()

                self.question_id_count = len([(question.keys()) for question in data['data']['questions']])

                # Print the count
                print(f'Total number of questions in this rounds are : {self.question_id_count}')

                
            else:
                print(f"Play API failed with status code: {play_response.status_code}", play_response.json())
        
    def quiz_Answer_API(self, question_id, answer_payload, User_instance,Register_instance): #answer
            Answer_URL = f"{Users.baseURL}api/v2/quiz/{Register_instance.quiz_entity_id}/answer/{question_id}"
            print("ANswer URL is =", Answer_URL)
                
            Second_AnsURLresponse = User_instance.client.post(
                url=Answer_URL,
                name='Second Answer',
                timeout=120,
                allow_redirects=False,
                headers={'Accept': 'application/json, text/plain, */*', 'Authorization': f'Bearer {User_instance.access_token}'},
                data= answer_payload
            )

            if Second_AnsURLresponse.status_code == 200:
                Second_AnsURLresponse_data = Second_AnsURLresponse.json()
                print(f"Answer response data is = ",Second_AnsURLresponse_data)

            else:
                print(f"Second_AnsURLresponse is failed with {Second_AnsURLresponse.status_code}", Second_AnsURLresponse.json())

    def Question_API(self, question_id, User_instance,Register_instance):
            Question_URL = f"{Users.baseURL}api/v2/quiz/{Register_instance.quiz_entity_id}/question/{question_id}"
            
            question_response = User_instance.client.get(
                url= Question_URL,
                name='Question_API',
                timeout=120,
                allow_redirects=False,
                headers={'Accept': 'application/json, text/plain, */*', 'Authorization': f'Bearer {User_instance.access_token}'}
            )

            if question_response.status_code == 200:
                self.question_Api_data = question_response.json()
                print("Question id = ",self.question_Api_data['data']['question']['id'])
            else:
                print("REsp[onse is , ",question_response.json())
                print(f"Question_API failed with status code: {question_response.status_code}")

    def play_Quiz(self, User_instance, Register_instance):

            if self.question_id_count == 1:

                firstQuestion = self.playApi_data['data']["currentQuestion"] #get first question data to use in first answer payload
                # self.currentQuestion['answer'] = play_data['details']['choice1']
                        
                firstQuestion['answer'] = self.playApi_data['data']["currentQuestion"]['details']['choice1']
                answer_payload = firstQuestion #store the response on new variable
                print("Question id = ",self.playApi_data['data']["currentQuestion"]['question_id'])
                self.quiz_Answer_API(self.playApi_data['data']["currentQuestion"]['question_id'], answer_payload, User_instance, Register_instance)

            elif self.question_id_count > 1 :
                
                firstQuestion = self.playApi_data['data']["currentQuestion"] #get first question data to use in first answer payload
                # self.currentQuestion['answer'] = play_data['details']['choice1']
                        
                firstQuestion['answer'] = self.playApi_data['data']["currentQuestion"]['details']['choice1']
                answer_payload = firstQuestion #store the response on new variable
                print("Question id = ",self.playApi_data['data']["currentQuestion"]['question_id'])
                self.quiz_Answer_API(self.playApi_data['data']["currentQuestion"]['question_id'], answer_payload, User_instance, Register_instance)
            
                for question_id in range(1, self.question_id_count):
                    self.Question_API(self.question_ID[question_id]['question_id'], User_instance, Register_instance)
                    play_data = self.playApi_data["data"]["questions"]

                    if play_data:
                        currentQuestion = self.playApi_data['data']['questions'][question_id] #get first question data to use in first answer payload
                        currentQuestion['answer'] = self.question_Api_data['data']["question"]['choice1']
                        answer_payload = currentQuestion
                        self.quiz_Answer_API(self.question_ID[question_id]['question_id'], answer_payload, User_instance, Register_instance)

                        time.sleep(4)

            else:
                print("No Questions present in Quiz round")
                
            #finish API's

            finish_payload = {
                "finish_type": "finishBtn",
                "is_demo": False
                }

            finish_response = User_instance.client.post(
                url= f"{Users.baseURL}api/v2/quiz/{Register_instance.quiz_entity_id}/finish",
                name='Finish Api',
                timeout=120,
                allow_redirects=False,
                headers={'Accept': 'application/json, text/plain, */*', 'Authorization': f'Bearer {User_instance.access_token}'},
                json=finish_payload,
            )


            if finish_response.status_code == 200:
                finish_response_data = finish_response.json()['data']
                
                print(f"Finish API response data = ",finish_response_data)

            else:
                print(f"Finish API failed with status code: {finish_response.status_code}")

            
            #update review

            Review_payload = {
                "review": "Load feedback",
                "rating": 5
                }

            Review_response = User_instance.client.post(
                url= f"{Users.baseURL}api/v2/quiz/{Register_instance.quiz_entity_id}/update-review",
                name='Review Api',
                timeout=120,
                allow_redirects=False,
                headers={'Accept': 'application/json, text/plain, */*', 'Authorization': f'Bearer {User_instance.access_token}'},
                json=Review_payload,
            )


            if Review_response.status_code == 200:
                Review_response_data = Review_response.json()['data']
                
                print(f"Review API response data = ",Review_response_data)

            else:
                print(f"Review API failed with status code: {Review_response.status_code}")
