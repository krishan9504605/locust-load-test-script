import json
import time
from os import access, name
from socket import timeout
from locust import HttpUser, SequentialTaskSet, task, between
import csv
from csvreader import CSVReader
import random

#Asssssment URL = https://dev2.dare2compete.com/recruiter/assessment/3928/assessment-edit

email_reader = CSVReader("Assessment_Users.csv")  # Assuming CSVReader is implemented
baseURL = 'https://dev2.dare2compete.com/'
assessment_round_id = 6106


def seq_task(_):
    return task

class MyUserTasks(SequentialTaskSet):
    
    
    def Answer_API(self, question_id, answer_payload):   #answer
        AnsURL = f"{baseURL}api/assessment2/assessment/{assessment_round_id}/answer/{question_id}"
            
        AnsURL_response = self.client.post(
            url=AnsURL,
            name='Answer API',
            timeout=120,
            allow_redirects=False,
            headers={'Accept': 'application/json, text/plain, */*', 'Authorization': f'Bearer {self.access_token}'},
            data=answer_payload
        )

        if AnsURL_response.status_code == 200:
            Ans_data = AnsURL_response.json()  
            
            print(f"Answer_API response data is = ",Ans_data)

        else:
            print(f"Answer_API response is failed with {AnsURL_response.status_code}")

    def Question_API(self, question_id):
        Question_URL = f"{baseURL}api/assessment2/assessment/{assessment_round_id}/question/{question_id}"
        
        question_response = self.client.get(
            url= Question_URL,
            name='Question_API',
            timeout=120,
            allow_redirects=False,
            headers={'Accept': 'application/json, text/plain, */*', 'Authorization': f'Bearer {self.access_token}'}
        )

        if question_response.status_code == 200:
            self.question_Api_data = question_response.json()
            print("Question id = ",self.question_Api_data['data']['question']['id'])
            
        else:
            print(f"Question_API", self.question_Api_data['data']['question']['id'], "failed with status code:", {question_response.status_code})
   
    def Assessment_URL_API(self):
        
        Assessment_URL = f"{baseURL}api/assessment2/assessment/{assessment_round_id}"

        #Assessment page api start here

        assessment_URL_response = self.client.get(
            url=Assessment_URL,
            name = 'Assessment ID API',
            timeout=120,
            allow_redirects=False,
            headers={'Accept': 'application/json, text/plain, */*'}
        )

        if assessment_URL_response.status_code == 200:
            #self.Assessment_data = assessment_URL_response.json()['data'] #get second question data to use in first answer payload
            print("Assessment_URL_API working.!")  

        else:
            print(f"Assessment_URL API failed with status code: {assessment_URL_response.status_code}")
            pass

    def register_API(self, email, name):
        
        #Reg API start:
        Reg_payload = {
        "id": None,
        "name": name,
        "email": email,
        "organisationName": "S INFINITY",
        "others_course_specialization": None,
        "player_type": 1,
        "gender": "M",
        "location": "India",
        "differently_abled": 0,
        "formValidStatus": True,
        "user_id": None,
        "organisation_id": 105107,
        "organisation": {
            "id": 105107,
            "name": "S INFINITY",
            "organisation_type": "1"
        },
        "user_type": 2,
        "work_experience": 9
        }

        Reg_URL = f"{baseURL}api/assessment2/assessment/register/{assessment_round_id}"

        registration_response = self.client.post(
            url=Reg_URL,
            name='Registration',
            timeout=120,
            allow_redirects=False,  
            headers={'Accept': 'application/json, text/plain, */*'},
            json=Reg_payload
        )

        if registration_response.status_code == 200:
            self.access_token = registration_response.json()['data']['encData']['access_token'] #fetch the access token variable
            user_id = registration_response.json()['data']['user_id']
            print(registration_response.json()['message'],"with email",registration_response.json()['data']['email'],f", and User ID is: ",user_id)
            # print("Token =",self.access_token)
            time.sleep(5)
           

        else:
            print(f"Registration failed with status code: {registration_response.status_code}")
        
        

    
    def play_Api(self):

        Play_URL = f"{baseURL}api/assessment2/assessment/{assessment_round_id}/play?"
        
        #Play Api start here:
        Play_URL_response = self.client.get(
                url=Play_URL,
                name = 'Play API',
                timeout=120,
                allow_redirects=False,
                headers={'Accept': 'application/json, text/plain, */*', 'Authorization': f'Bearer {self.access_token}'}
            )

        if Play_URL_response.status_code == 200:
            self.playApi_data = Play_URL_response.json()
            self.question_ID = Play_URL_response.json()['data']['questions']
            
            # Convert the JSON-formatted string to a Python dictionary
            data = Play_URL_response.json()

            self.question_id_count = len([(question.keys()) for question in data['data']['questions']])

            # Print the count
            print(f'Total number of questions in this rounds are : {self.question_id_count}')
            time.sleep(5)


        else:
            print(f"Play API failed with status code: {Play_URL_response.status_code}")
            print("Play API Response is:", Play_URL_response.json() )
    
    def play_Quiz(self):
        #answer api

        answers_choices = ["choice1", "choice2", "choice3"]
        # Choose a random key from the list of choices_keys
        random_choice_key = random.choice(answers_choices)

        if self.question_id_count == 1:

            firstQuestion = self.playApi_data['data']["currentQuestion"] #get first question data to use in first answer payload
            firstQuestion['answer'] = self.playApi_data['data']["currentQuestion"]['details'][random_choice_key]
            answer_payload = firstQuestion #store the response on new variable
            print("Question id = ",self.playApi_data['data']["currentQuestion"]['question_id'])
            self.Answer_API(self.playApi_data['data']["currentQuestion"]['question_id'], answer_payload)

        elif self.question_id_count > 1 :

            firstQuestion = self.playApi_data['data']["currentQuestion"] #get first question data to use in first answer payload
            firstQuestion['answer'] = self.playApi_data['data']["currentQuestion"]['details'][random_choice_key]
            answer_payload = firstQuestion #store the response on new variable
            print("Question id = ",self.playApi_data['data']["currentQuestion"]['question_id'])
            self.Answer_API(self.playApi_data['data']["currentQuestion"]['question_id'], answer_payload)

        
            for question_id in range(1, self.question_id_count):
                self.Question_API(self.question_ID[question_id]['question_id'])
                play_data = self.playApi_data["data"]["questions"]

                if play_data:
                    currentQuestion = self.playApi_data['data']['questions'][question_id] #get first question data to use in first answer payload
                    currentQuestion['answer'] = self.question_Api_data['data']["question"][random_choice_key]
                    answer_payload = currentQuestion
                    self.Answer_API(self.question_ID[question_id]['question_id'], answer_payload)

                    time.sleep(4)
            time.sleep(5)
            
        else:
            print("No Questions present in this Assessment round")

        
        #finish API's

        finish_payload = {
            "finish_type": "finishBtn",
            "is_demo": False
            }

        finish_response = self.client.post(
            url= f"{baseURL}api/assessment2/assessment/{assessment_round_id}/finish",
            name='Finish Api',
            timeout=120,
            allow_redirects=False,
            headers={'Accept': 'application/json, text/plain, */*', 'Authorization': f'Bearer {self.access_token}'},
            json=finish_payload,
        )


        if finish_response.status_code == 200:
            finish_data = finish_response.json()['data']
            
            print(f"Finish API response data = ",finish_data)
            time.sleep(5)

        else:
            print(f"Finish API failed with status code: {finish_response.status_code}")

        
        #update review

        Review_payload = {
            "review": "Load feedback",
            "rating": 5
            }

        Review_response = self.client.post(
            url= f"{baseURL}api/assessment2/assessment/{assessment_round_id}/update-review",
            name='Review Api',
            timeout=120,
            allow_redirects=False,
            headers={'Accept': 'application/json, text/plain, */*', 'Authorization': f'Bearer {self.access_token}'},
            json=Review_payload,
        )


        if Review_response.status_code == 200:
            Review_data = Review_response.json()['data']
            
            print(f"Review API response data = ",Review_data)
            time.sleep(5)

        else:
            print(f"Review API failed with status code: {Review_response.status_code}")


    @seq_task(1)
    def play_assessment(self):
        #pass values from csv
            
        csvemail = next(email_reader)                 
        email = csvemail[0]
        name = csvemail[1]

        # Login
        self.Assessment_URL_API()

        # Register (you may need to adapt this part based on your API)
        self.register_API(email, name)

        #Play_verify API
        self.play_Api()

        #Play
        self.play_Quiz()
            
        
        
class MyUser(HttpUser):
    wait_time = between(5,10)
    tasks = [MyUserTasks]
    host = f'{baseURL}'