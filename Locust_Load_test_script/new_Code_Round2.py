import email
from email.message import EmailMessage
import json
import time
import requests
import datetime
import random
from locust import HttpUser, SequentialTaskSet, task, between, constant_pacing
import os
import re
import csv
# from locust_plugins.csvreader import CSVReader
from locust_plugins.csvreader import CSVReader
from locust import HttpUser, SequentialTaskSet, task, between, constant_pacing, events
from locust.runners import STATE_STOPPING, STATE_STOPPED, STATE_CLEANUP
from requests.exceptions import HTTPError

fileName = os.path.basename(__file__)
numbers = re.findall('[0-9]+', fileName)
email_reader = CSVReader("/mnt/locust/file" + numbers[0] + ".csv")

def seq_task(_):
    return task

def fetch_emails_from_csv_url(csv_url):
    response = requests.get(csv_url)
    response.raise_for_status()  # Ensure we notice bad responses
    emails = []
    # Decode the content and parse CSV
    decoded_content = response.content.decode('utf-8').splitlines()
    reader = csv.reader(decoded_content)
    for row in reader:
        emails.append(row[0])  # Assuming email is in the first column
    return emails

def CurrentTimeZone():
    # Get current datetime in the desired timezone
    formatted_current_dt = datetime.datetime.now(datetime.timezone(datetime.timedelta(hours=5, minutes=30))).strftime('%Y-%m-%d %H:%M:%S%z')

    # Insert colon between hours and minutes in the timezone offset
    formatted_current_dt = formatted_current_dt[:-2] + ':' + formatted_current_dt[-2:]

    return formatted_current_dt # Output: Current date and time in the specified format

def create_event_data(round_id, sEventType):
    Event_data = {
        "iEntityId": round_id,
        "sEntityType": "App\\Model\\CodeContests",
        "sEventType": sEventType,
        "sDateTime": CurrentTimeZone()
    }
    return Event_data


class MyUser(SequentialTaskSet):
    baseURL = 'https://unstop.com/'
    code_round_id =  51791 #6996 #11969  # 18976
    code_id = 394940
    compId = 50340
    sleep = 1

    def __init__(self, *args, **kwargs):
       
        # csvemail = next(email_reader)
        # self.emails = csvemail[0]
        # self.mobile = csvemail[2]
        super().__init__(*args, **kwargs)
        self.access_token = None
        self.sleep_time = 3
        self.code_solution1 = {
                        "source": "#include <bits/stdc++.h>\nusing namespace std;\n \nint minCost(vector<long long int> vec)\n{           \nlong  long   int n=vec.size();\npriority_queue<long long int,vector<long long int>,greater<long long int>> pq; for(int i=0; i<n; ++i) pq.push(vec[i]);\nint cost=0; while(!pq.empty()) {\nint x=pq.top(); pq.pop(); if(!pq.empty()) {\nint y=pq.top(); pq.pop(); cost+=(x+y); pq.push(x+y);\n}\n}\nreturn cost;\n}\n \nint main()\n{\n long long int n;\ncin>>n;\nvector<long long int> vec(n);\nfor(int i=0;i<n;i++)\n{\ncin>> vec[i];\n}\ncout<<minCost(vec)<<endl;\nreturn 0;\n}",
                        "lang": "2"
                        }
        self.code_solution2 = {
                        "source": "#include <bits/stdc++.h>\n \n#define endl \"\\n\"\n \nusing namespace std;\n \nint getMaxLength(vector<int> &A, int N){\n   \n\tvector<long long> prefSums(N+2);\n \n\tfor( int i = 1; i <= N; i++ ) prefSums[i] = prefSums[i-1] + A[i];\n \n\tvector<int> dpMaxLength(N+2, 0), optimalPartition(N+2, 0);\n   \n\tfor( int i = 1; i <= N; i++ ){\n    \toptimalPartition[i] = max(optimalPartition[i-1], optimalPartition[i] );\n    \tdpMaxLength[i] = 1 + dpMaxLength[optimalPartition[i]];\n \n    \tlong long nextSum = (prefSums[i] - prefSums[optimalPartition[i]]) + prefSums[i];\n    \tint nextIdx = lower_bound(prefSums.begin() + 1, prefSums.begin() + N + 1, nextSum) - prefSums.begin();\n    \tif( nextIdx <= N ) optimalPartition[nextIdx] = max(i, optimalPartition[nextIdx]);\n\t}\n \n\treturn dpMaxLength[N];\n}\n \nint main(){\n\tios_base::sync_with_stdio(false);\n\tcin.tie(NULL);\n\tint N; cin >> N;   \n\tvector<int> v(N+2);\n\tfor( int i = 1; i <= N; i++ ) cin >> v[i];\n \n\tcout << getMaxLength(v, N) << endl;\n   \n\treturn 0;\n}",
                        "lang": "2"
                        }
        self.code_solution3 = {
                        "source": "#include \"bits/stdc++.h\" \nusing namespace std; \n#define int long long int\nint32_t main()\n{\nint n; cin>>n; priority_queue<int,vector<int>,greater<int>> pq; int ans=0, vaccines=0, health=0;\nfor(int i=0; i<n; ++i) { int x; cin>>x; health+=x; vaccines++; pq.push(x);\nif(health>=0) ans=max(ans,vaccines); while(health<0) {\nhealth-=pq.top(); pq.pop();\nvaccines--;\n}\n}\ncout<<max(vaccines,ans);\n}",
                        "lang": "2"
                        }
        self.login_API()
        
        # self.emails = None

    #Login API's
        
    def login_API(self):
        csvemail = next(email_reader)
        emails = csvemail[0]  # Adjust indexing as needed for your use case
        # Your login logic here
        for i in range(0, 5):
            login_url = f"{self.baseURL}api/user/login"

            login_data = {
                "grant_type": "password",
                "username": "dummy25jgebt6945",
                "email": emails,  # Assuming email is hardcoded for now
                "password": "abcdef",
                "scope": "*",
                "network": "",
                "access_token": "",
            }

            login_response = self.client.post(
                url=login_url,
                name='1. Login',
                timeout=120,
                allow_redirects=False,
                headers={'Accept': 'application/json, text/plain, */*'},
                json=login_data
            )
            # print('status code is =', login_response.status_code)
            # print('ur email is  = ',emails)
            if login_response.status_code == 200 and 'user' in login_response.json() and 'id' in login_response.json()['user']:
                time.sleep(self.sleep_time)
                self.access_token = login_response.json()['access_token']
                self.player_id = login_response.json()['user']['id']
                break
            elif i < 4:
                print("Login_API Failed at" + ' time-' + str(i), login_response.text)
                
            else:
                print("Login_API Failed permanently at ", login_response.text)
                self.user.environment.events.request_failure.fire(
                request_type="POST",
                name="1. Login",
                response_time=login_response.elapsed.total_seconds() * 1000,
                response_length=len(login_response.content),
                exception=Exception(f"Login_API Failed permanently at: {login_response.text}"),
                )
                time.sleep(999999999990)

    def opp_API(self):

        for i in range(0, 5):
            opp_url = f"{self.baseURL}api/public/competition/{self.compId}"
            opp_response = self.client.get(
                url=opp_url,
                name='2. OppPage_API',
                timeout=120,
                allow_redirects=False,
                headers={'Accept': 'application/json, text/plain, */*', 'Authorization': f'Bearer {self.access_token}'}
            )

            if opp_response.status_code == 200 and 'data' in opp_response.json():
                time.sleep(self.sleep_time)
                # oppPage = opp_response.json()
                # # Convert the string to a Python dictionary
                
                # # Iterate through each item in the API response
                # for item in oppPage['data']['competition']['rounds']:
                #     # Check if the 'entity_type' is 'App\\Model\\Quiz'
                #     if item.get('entity_type') == 'App\\Model\\Quiz':
                #         # Extract and store 'id' and 'entity_id'
                #         self.quiz_id = item.get('id') #611652
                #         self.quiz_entity_id = item.get('entity_id') #247191
                #         break
                #         # Break the loop once the first matching item is found
                    
                #     elif item.get('entity_type') == 'App\\Model\\CodeContests':
                #         print('Entering in Code Round')
                #         # Extract and store 'id' and 'entity_id'
                #         self.code_id = item.get('id') #615908
                #         self.code_entity_id = item.get('entity_id') #112940
                #         break
                #         # Break the loop once the first matching item is found
                #     else:
                #         print("No Code & Quiz Round is present in this opp")
                #         # time.sleep(999999999990)
        
                # self.proctoring_data = opp_response.json()['data']['competition']['regnRequirements']['proctoring']
                break

            elif i < 4:
                print(f"opp_url API failed " + ' time-' + str(i), {opp_response.text})
                time.sleep(2)

            else:
                print("opp_url API permanently failed " + ' time-' + str(i), opp_response.text)
                time.sleep(999999999990)

    def verify_code_playApi(self):

        for i in range(0, 5):
            PlayStatus_Url = f"{self.baseURL}api/opportunity/{self.compId}/associations/{self.code_id}?withPlayStatus=true"
            # print("URL is = ,",play_URL)

            PlayStatus_response = self.client.get(
                url=PlayStatus_Url,
                name='3. PlayStatus_API',
                timeout=120,
                allow_redirects=False,
                headers={'Accept': 'application/json, text/plain, */*', 'Authorization': f'Bearer {self.access_token}'}
            )

            if PlayStatus_response.status_code == 200:
                time.sleep(self.sleep_time)
                # print("PlayStatus_Url API response working")
                break

            elif i < 4:
                print(f"PlayStatus_Url API Permanently failed " + ' time-' + str(i), {PlayStatus_response.text})
                pass

            else:
                print(f"PlayStatus_Url API failed " + ' time-' + str(i), {PlayStatus_response.text})
                time.sleep(2)
    
    def play_code_API(self):

        for z in range(0, 5):
            play_code_API_URL = f"{self.baseURL}api/code-contests/{self.code_round_id}/play"
            
            play_code_API_response = self.client.get(
                url= play_code_API_URL,
                name=f'4. Play_code_API',
                timeout=120,
                allow_redirects=False,
                headers={'Accept': 'application/json, text/plain, */*', 'Authorization': f'Bearer {self.access_token}'}
            )

            if play_code_API_response.status_code == 200 and 'data' in play_code_API_response.json():
                # Extracting challenge ids
                self.Quesid = [challenge['id'] for challenge in play_code_API_response.json()['data']['contestinfo']['challenges']]
                self.relation_ids = [challenge['relation_id'] for challenge in play_code_API_response.json()['data']['contestinfo']['challenges']]
                # print('DATGA iss = ',self.Quesid,self.relation_ids)
                # self.first_Qid = play_code_API_response.json()['data']["contestinfo"]['challenges'][0]['id']
                # self.first_relid = play_code_API_response.json()['data']["contestinfo"]['challenges'][0]['relation_id']
                # self.second_Qid = play_code_API_response.json()['data']["contestinfo"]['challenges'][1]['id']
                # self.second_relid = play_code_API_response.json()['data']["contestinfo"]['challenges'][1]['relation_id']
                # self.third_Qid = play_code_API_response.json()['data']["contestinfo"]['challenges'][2]['id']
                # self.third_relid = play_code_API_response.json()['data']["contestinfo"]['challenges'][2]['relation_id']
                time.sleep(self.sleep_time)
                break
               
            elif z < 4:
                print(f"get_review_setting API failed"  + " time=" + str(z), {play_code_API_response.text})
                time.sleep(self.sleep)

            else:
                print("get_review_setting API response is failed Permanently " + " time=" + str(z), play_code_API_response.text)
                time.sleep(9999999000000)

    def save_proctoring_API(self):

        Proct_data_ip = {
            "association_id": self.code_id,
            "round_type": "App\\Model\\CodeContests",
            "ipaddress": True
            }

        for z in range(0, 5):

            Proct_URL = f"{self.baseURL}api/proctoring/save-proctoring-data"

            proct_response = self.client.post(
                url=Proct_URL,
                name= '5. save_proctoring_API',
                timeout=120,
                allow_redirects=False,  
                headers={'Accept': 'application/json, text/plain, */*', 'Authorization': f'Bearer {self.access_token}'},
                json=Proct_data_ip
            )

            if proct_response.status_code == 200 and 'status' in proct_response.json():
                time.sleep(self.sleep_time)
                break
            
            elif z < 4:
                print(f"Proctoring_save API failed "  + "time=" + str(z), {proct_response.text})
                time.sleep(self.sleep)

            else:
                print(f"Proctoring_save API Permanent failed due to : {proct_response.text}")
                time.sleep(9999999000000)

    def event(self,json_data, API_no):

        for z in range(0, 5):

            Event_URL = f"{self.baseURL}api/proctoring/save/round/event"

            event_response = self.client.post(
                url=Event_URL,
                name= f'{API_no}. Event_API',
                timeout=120,
                allow_redirects=False,  
                headers={'Accept': 'application/json, text/plain, */*', 'Authorization': f'Bearer {self.access_token}'},
                json=json_data
            )

            if event_response.status_code == 200 and 'status' in event_response.json():
                time.sleep(self.sleep_time)
                break
            
            elif z < 4:
                print(f"Event_API failed "  + "time=" + str(z), {event_response.text})
                time.sleep(self.sleep)

            else:
                print(f"Event_API API Permanent failed due to : {event_response.text}")
                time.sleep(9999999000000)

    def get_Time_info_API(self,API_no):

        for i in range(0, 5):
            Time_info_API = f"{self.baseURL}api/code-contests/{self.code_round_id}/get-time-info"
            Time_info_API_response = self.client.get(
                url=Time_info_API,
                name= f'{API_no}. get_Time_info_API',
                timeout=120,
                allow_redirects=False,
                headers={'Accept': 'application/json, text/plain, */*', 'Authorization': f'Bearer {self.access_token}'}
            )

            if Time_info_API_response.status_code == 200 and 'data' in Time_info_API_response.json():
                time.sleep(self.sleep_time)
                # print("opp_url API response working")
                break
            elif i==4 :
                print(f"opp_url API Permanently Failed " + ' time-' + str(i),{Time_info_API_response.text})
                pass
            else:
                print(f"opp_url API failed " + ' time-' + str(i), {Time_info_API_response.text})
                time.sleep(2)
    
    def use_updated_compiler(self, Qid , relid, solutions, API_no):

        for z in range(0, 5):

            updated_compiler_URL = f"{self.baseURL}api/code-contests/{Qid}/submit/{relid}?use_updated_compiler=false"
            query_params = {'use_updated_compiler': False}
            updated_compiler_response = self.client.post(
                url=updated_compiler_URL,
                name= f'{API_no}. Use_updated_compiler',
                timeout=120,
                allow_redirects=False,  
                headers={'Accept': 'application/json, text/plain, */*', 'Authorization': f'Bearer {self.access_token}'},
                params = query_params,
                data = solutions
                )      
            
            if updated_compiler_response.status_code == 200 and 'data' in updated_compiler_response.json():
                self.submission_id = updated_compiler_response.json()['submission']['id']
                time.sleep(self.sleep_time)
                break
            
            elif z < 4:
                print(f"Event_API failed "  + "time=" + str(z), {updated_compiler_response.text})
                time.sleep(self.sleep)

            else:
                print(f"Event_API API Permanent failed due to : {updated_compiler_response.text}")
                time.sleep(9999999000000)

    def check_testcases(self,submis_id,relid, API_no):

        for i in range(0, 5):
            check_testcases_URL = f"{self.baseURL}api/code-contests/submissions/{submis_id}/check-submission-testcases/relation_id/{relid}"
            check_testcases_response = self.client.get(
                url=check_testcases_URL,
                name= f'{API_no}. check-submission-testcases',
                timeout=120,
                allow_redirects=False,
                headers={'Accept': 'application/json, text/plain, */*', 'Authorization': f'Bearer {self.access_token}'}
            )

            if check_testcases_response.status_code == 200 and 'data' in check_testcases_response.json():
                time.sleep(self.sleep_time)
                break
            elif i==4 :
                print(f"opp_url API Permanently Failed " + ' time-' + str(i),{check_testcases_response.text})
                pass
            else:
                print(f"opp_url API failed " + ' time-' + str(i), {check_testcases_response.text})
                time.sleep(2)

    def get_user_submission(self,Ques_id,rel_id, API_no):

        for z in range(0, 5):
            get_user_submission_URL = f"{self.baseURL}api/code-contests/{Ques_id}/get-user-submissions/{rel_id}"
            
            get_user_submission_response = self.client.get(
                url= get_user_submission_URL,
                name=f'{API_no}. Get_user_submission',
                timeout=120,
                allow_redirects=False,
                headers={'Accept': 'application/json, text/plain, */*', 'Authorization': f'Bearer {self.access_token}'}
            )

            if get_user_submission_response.status_code == 200 and 'data' in get_user_submission_response.json():
                time.sleep(self.sleep)
                break
               
            elif z < 4:
                print(f"get_review_setting API failed"  + " time=" + str(z), {get_user_submission_response.text})
                time.sleep(self.sleep)

            else:
                print("get_review_setting API response is failed Permanently " + " time=" + str(z), get_user_submission_response.text)
                time.sleep(9999999000000)

    """
    # def code_Answer_API(self, y): #answer
    #     for i in range(0, 5):
    #     # print("ISSSs isss ==",self.question_ID[0]['relation_id'])
    #         rel_id = self.question_ID_code[0]['relation_id']
            
    #         Answer_URL = f"{self.baseURL}api/code-contests/326142/submit/{rel_id}"

    #         answer_payload = {
    #             "source": self.code_solution,
    #             "lang": "2"
    #             }
                            
    #         Answer_URL_response = self.client.post(
    #             url=Answer_URL,
    #             name=f'{7 + y}. Code Answer API - {y}',
    #             timeout=120,
    #             allow_redirects=False,
    #             headers={'Accept': 'application/json, text/plain, */*', 'Authorization': f'Bearer {self.access_token}'},
    #             data= answer_payload
    #         )

    #         if Answer_URL_response.status_code == 200:
    #             time.sleep(self.sleep_time)
    #             # print(f"code_Answer_API_data is working with status code {Answer_URL_response.status_code}")
    #             break

    #         elif i == 4:
    #             print(f"Second_AnsURLresponse is Permanently failed " + ' time-' + str(i), {Answer_URL_response.text})
    #             pass

    #         else:
    #             print(f"Second_AnsURLresponse is failed " + ' time-' + str(i), {Answer_URL_response.text})
    #             time.sleep(2) 

    """

    def finish_code_API(self):
        #finish API
        for i in range(0, 5):
            finish_URL = f"{self.baseURL}api/code-contests/{self.code_round_id}/finish"

            finish_payload = {
            "finish_type": "finishBtn"
            }

            finish_response = self.client.post(
                url= finish_URL,
                name='22. finish_code_API',
                timeout=120,
                allow_redirects=False,
                headers={'Accept': 'application/json, text/plain, */*', 'Authorization': f'Bearer {self.access_token}'},
                json=finish_payload,
            )

            if finish_response.status_code == 200 and 'data' in finish_response.json():
                time.sleep(self.sleep_time)
                break

            elif i < 4:
                print(f"Finish API Permanently failed " + ' time-' + str(i), {finish_response.text})
                pass

            else:
                print(f"Finish API failed " + ' time-' + str(i), {finish_response.text})
                time.sleep(2)

        #update review
        for i in range(0, 5):
            Review_payload = {
                "review": "Load code round feedback",
                "rating": 5
                }

            Review_response = self.client.post(
                url= f"{self.baseURL}api/code-contests/{self.code_round_id}/update-review",
                name='23. Review code round Api',
                timeout=120,
                allow_redirects=False,
                headers={'Accept': 'application/json, text/plain, */*', 'Authorization': f'Bearer {self.access_token}'},
                json=Review_payload,
            )

            if Review_response.status_code == 200:
                time.sleep(self.sleep_time)
                # Review_response_data = Review_response.json()['data']
                # print(f"Review API feedback is =",Review_response_data['content'])
                break

            elif i < 4:
                print(f"Review API Permanently failed " + ' time-' + str(i),  {Review_response.text})
                pass

            else:
                print(f"Review API failed " + ' time-' + str(i),  {Review_response.text})
                time.sleep(2)


    @seq_task(1)
    def Load_test1(self):
        
        
        
        self.opp_API()
        self.verify_code_playApi()
        self.play_code_API()
        self.save_proctoring_API()
        self.event(create_event_data(self.code_round_id, "join_exit_panel"),'6')
        self.get_user_submission(self.Quesid[0],self.relation_ids[0],'7')
        self.use_updated_compiler(self.Quesid[0], self.relation_ids[0], self.code_solution1,'8')
        # time.sleep(3)
        self.check_testcases(self.submission_id,self.relation_ids[0],'9')
        # time.sleep(3)
        self.get_user_submission(self.Quesid[0],self.relation_ids[0],'10')
        self.get_user_submission(self.Quesid[1],self.relation_ids[1],'11')
        self.use_updated_compiler(self.Quesid[1], self.relation_ids[1], self.code_solution2,'12')
        # time.sleep(3)
        self.check_testcases(self.submission_id,self.relation_ids[1],'13')
        self.get_user_submission(self.Quesid[1],self.relation_ids[1],'14')
        self.get_Time_info_API('15')
        self.get_user_submission(self.Quesid[2],self.relation_ids[2],'16')
        
        self.use_updated_compiler(self.Quesid[2], self.relation_ids[2], self.code_solution3, '17')
        self.check_testcases(self.submission_id,self.relation_ids[2],'18')
        # time.sleep(3)
        self.get_user_submission(self.Quesid[2],self.relation_ids[2],'19')
        self.get_Time_info_API('20')
        self.event(create_event_data(self.code_round_id, "finish_test"),'21')
        self.finish_code_API()
        print("Finished..!")

        time.sleep(9999999999999)
        

class LocustForTesting(HttpUser):
    tasks = [MyUser]
    host = f'{MyUser.baseURL}'
    wait_time = constant_pacing(400)
    # wait_time = between(1, 2)
    weight = 1
    min_wait = 10
    max_wait = 190

        