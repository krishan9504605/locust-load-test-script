import email
import time
from time import sleep
from new_User import Users
from new_Reg import Register
from new_Quiz_Round import Quiz
from new_Code_Round import Code
from locust import HttpUser, SequentialTaskSet, task, between, constant_pacing
from csvreader import CSVReader
from locust.exception import StopUser
import os
import re
import csv
import requests
from locust import HttpUser, SequentialTaskSet, task, between, constant_pacing, events
from locust.runners import STATE_STOPPING, STATE_STOPPED, STATE_CLEANUP
from requests.exceptions import HTTPError


# email_reader = CSVReader("User_Details.csv")  # Assuming CSVReader is implemented
fileName = os.path.basename(__file__)
numbers = re.findall('[0-9]+', fileName)

# sleep_time = 10



def seq_task(_):
     return task

def fetch_emails_from_csv_url(csv_url):
    response = requests.get(csv_url)
    response.raise_for_status()  # Ensure we notice bad responses
    emails = []
    # Decode the content and parse CSV
    decoded_content = response.content.decode('utf-8').splitlines()
    reader = csv.reader(decoded_content)
    for row in reader:
        emails.append(row[0])  # Assuming email is in the first column
    return emails

class MyUser(SequentialTaskSet):
        
        
    @seq_task(1)
    def Load_test(self):
            
            #pass values from csv
            # Fetch emails from the CSV URL
            csv_url = "https://d8it4huxumps7.cloudfront.net/files/664338d5e1da6_file0.csv"
            emails_list = fetch_emails_from_csv_url(csv_url)

            # Use the first email for this instance
            emails = emails_list[0]
            # csvemail = next(email_reader)                 
            # emails = csvemail[0]
            # mobile = csvemail[2]
            
            # Initialize the obj for all_funct instance with the appropriate client
            self.User_instance = Users(client=self.client, emails=emails) #This will call login func ready before starting the execution
            self.Register_instance = Register()
            self.Quiz_instance = Quiz()
            self.Code_instance = Code()

             #Opp Page API
            self.Register_instance.opp_API(self.User_instance)

            # # Register (you may need to adapt this part based on your API)
            # self.Register_instance.register_API(emails, mobile,self.User_instance)
            
            """
            # #Play_verify API
            self.Quiz_instance.verify_quiz_playApi(self.User_instance,self.Register_instance)

            # # #Play (include Finish & Feedback api for quiz round only)
            self.Quiz_instance.play_Quiz(self.User_instance, self.Register_instance)

            """

            # # #Play_verify API for code
            self.Code_instance.verify_code_playApi(self.User_instance, self.Register_instance)

            # # #code Answer API
            self.Code_instance.code_Answer_API(self.User_instance)
            

            # # #get Time info API
            self.Code_instance.get_Time_info_API(self.User_instance, self.Register_instance)
            

            # # #get finish & feedback API
            self.Code_instance.finish_code_API(self.User_instance, self.Register_instance)
            
            print("Finished..!")
            time.sleep(9999999999999)        
            
    
class LocustForTesiting(HttpUser):
    tasks = [MyUser]
    host = f'{Users.baseURL}'
    wait_time = constant_pacing(400)
    # wait_time = between(1, 2)
    weight = 1
    min_wait = 10
    max_wait = 190